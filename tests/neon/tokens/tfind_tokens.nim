import std/[strutils]
import ../../../src/neon/token
import ../../../src/neon/pattern


## All tokens found
block:
  let
    regex = initTokensRegex(r"\{\{", r"\}\}")
    tmpl = """
    {{ variable }}
    {{># }}
    {{<# }}
    {{>if 123==$ }}
    {{>elif 123==$ }}
    {{>else }}
    {{>for i in $ }}
    {{<for }}
    {{ $.content }}
    """.dedent

    tokens = tmpl.findTokens(regex)


  doAssert tokens[0].kind == tkScopedVariable
  doAssert tokens[0].match == "{{ variable }}"
  doAssert tokens[0].position == 0..13
  doAssert tokens[1].kind == tkCommentStart
  doAssert tokens[1].match == "{{># }}\n"
  doAssert tokens[1].position == 15..22
  doAssert tokens[2].kind == tkCommentStop
  doAssert tokens[2].match == "{{<# }}\n"
  doAssert tokens[2].position == 23..30
  doAssert tokens[3].kind == tkIfStart
  doAssert tokens[3].match == "{{>if 123==$ }}\n"
  doAssert tokens[3].position == 31..46
  doAssert tokens[4].kind == tkElifStart
  doAssert tokens[4].match == "{{>elif 123==$ }}\n"
  doAssert tokens[4].position == 47..64
  doAssert tokens[5].kind == tkElseStart
  doAssert tokens[5].match == "{{>else }}\n"
  doAssert tokens[5].position == 65..75
  doAssert tokens[6].kind == tkForStart
  doAssert tokens[6].match == "{{>for i in $ }}\n"
  doAssert tokens[6].position == 76..92
  doAssert tokens[7].kind == tkForStop
  doAssert tokens[7].match == "{{<for }}\n"
  doAssert tokens[7].position == 93..102
  doAssert tokens[8].kind == tkContextVariable
  doAssert tokens[8].match == "{{ $.content }}"
  doAssert tokens[8].position == 103..117


## All tokens found
block:
  let
    regex = initTokensRegex(r"\[\[", r"\]\]")
    tmpl = """
    [[ variable ]]
    [[># ]]
    [[<# ]]
    [[>if 123==$ ]]
    [[>elif 123==$ ]]
    [[>else ]]
    [[>for i in $ ]]
    [[<for ]]
    [[ $.content ]]
    """.dedent

    tokens = tmpl.findTokens(regex)


  doAssert tokens[0].kind == tkScopedVariable
  doAssert tokens[0].match == "[[ variable ]]"
  doAssert tokens[0].position == 0..13
  doAssert tokens[1].kind == tkCommentStart
  doAssert tokens[1].match == "[[># ]]\n"
  doAssert tokens[1].position == 15..22
  doAssert tokens[2].kind == tkCommentStop
  doAssert tokens[2].match == "[[<# ]]\n"
  doAssert tokens[2].position == 23..30
  doAssert tokens[3].kind == tkIfStart
  doAssert tokens[3].match == "[[>if 123==$ ]]\n"
  doAssert tokens[3].position == 31..46
  doAssert tokens[4].kind == tkElifStart
  doAssert tokens[4].match == "[[>elif 123==$ ]]\n"
  doAssert tokens[4].position == 47..64
  doAssert tokens[5].kind == tkElseStart
  doAssert tokens[5].match == "[[>else ]]\n"
  doAssert tokens[5].position == 65..75
  doAssert tokens[6].kind == tkForStart
  doAssert tokens[6].match == "[[>for i in $ ]]\n"
  doAssert tokens[6].position == 76..92
  doAssert tokens[7].kind == tkForStop
  doAssert tokens[7].match == "[[<for ]]\n"
  doAssert tokens[7].position == 93..102
  doAssert tokens[8].kind == tkContextVariable
  doAssert tokens[8].match == "[[ $.content ]]"
  doAssert tokens[8].position == 103..117
