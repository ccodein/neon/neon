import std/[strutils]
import ../../../src/neon/token


## $ for the Tokens type
block:
  let token = Token(kind: tkScopedVariable, match: "{{ var }} \n", position: 0..9)

  doAssert $token == """
    kind: tkScopedVariable
    match: "{{ var }} \x0A"
    position: 0 .. 9""".dedent
