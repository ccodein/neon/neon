import std/[re, strutils]
import ../../../src/neon/pattern {.all.}


## for start token without spaces
block:
  var matches: array[4, string]

  let
    value = "{{>for i in $}}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&FOR_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{>for i in $}}"


## for start token without spaces
block:
  var matches: array[4, string]

  let
    value = "{{ > for i in variable }}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&FOR_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{ > for i in variable }}"


## for start token with appended spaces and newline
block:
  var matches: array[4, string]

  let
    value = "  {{>for i in $}}  \n"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&FOR_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "  {{>for i in $}}  \n"


