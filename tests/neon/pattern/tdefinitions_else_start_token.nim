import std/[re, strutils]
import ../../../src/neon/pattern {.all.}


## else start token
block:
  var matches: array[1, string]

  let
    value = "{{>else}}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&ELSE_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{>else}}"


## else start token with spaces
block:
  var matches: array[1, string]

  let
    value = "{{ > else }}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&ELSE_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{ > else }}"


## else start token with appended spaces and newline
block:
  var matches: array[1, string]

  let
    value = "{{>else}}  \n"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&ELSE_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{>else}}  \n"


## elif start token with appended und leading spaces, newline
block:
  var matches: array[1, string]

  let
    value = "  {{>else}}  \n"

    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&ELSE_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "  {{>else}}  \n"
