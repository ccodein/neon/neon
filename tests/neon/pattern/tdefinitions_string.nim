import std/[re]
import ../../../src/neon/pattern {.all.}


## correct string
block:
  var matches: array[2, string]

  let
    value = "\"saas1234_ds\""
    isMatch = value.match(re("(" & definitions & "(?&STRING))"), matches)

  doAssert isMatch
  doAssert matches[0] == "\"saas1234_ds\""


## no quotes
block:
  var matches: array[1, string]

  let
    value = "saas1234_ds"
    isMatch = value.match(re("(" & definitions & "(?&STRING))"), matches)

  doAssert not isMatch

