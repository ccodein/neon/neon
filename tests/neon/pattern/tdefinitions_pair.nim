import std/[re]
import ../../../src/neon/pattern {.all.}


## pair
block:
  var matches: array[1, string]

  let
    value = "\"key\": 123"
    isMatch = value.match(re("(" & definitions & "(?&PAIR))"), matches)

  doAssert isMatch
  doAssert matches[0] ==  "\"key\": 123"

## key not a string
block:
  var matches: array[1, string]

  let
    value = "key: 123"
    isMatch = value.match(re("(" & definitions & "(?&PAIR))"), matches)

  doAssert not isMatch
