import std/[re, strutils]
import ../../../src/neon/pattern {.all.}


## for stop token
block:
  var matches: array[1, string]

  let
    value = "{{<for}}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&FOR_STOP_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{<for}}"


## for stop token with spaces
block:
  var matches: array[1, string]

  let
    value = "{{ < for }}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&FOR_STOP_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{ < for }}"


## for stop token with appended and leading spaces, newline
block:
  var matches: array[1, string]

  let
    value = "  {{<for}}  \n"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&FOR_STOP_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "  {{<for}}  \n"
