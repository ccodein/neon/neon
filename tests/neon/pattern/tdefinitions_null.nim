import std/[re]
import ../../../src/neon/pattern {.all.}


## true
block:
  var matches: array[1, string]

  let
    value = "null"
    isMatch = value.match(re("(" & definitions & "(?&NULL))"), matches)

  doAssert isMatch
  doAssert matches[0] == "null"


## not null
block:
  var matches: array[1, string]

  let
    value = "some"
    isMatch = value.match(re("(" & definitions & "(?&NULL))"), matches)

  doAssert not isMatch
