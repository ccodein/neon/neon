import std/[re, strutils]
import ../../../src/neon/pattern {.all.}


## variable token
block:
  var matches: array[4, string]

  let
    value = "{{ variable }}"
    isMatch = value.match(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&TOKENS))"), matches)

  doAssert isMatch
  doAssert matches[0] == "{{ variable }}"


## comment start token
block:
  var matches: array[4, string]

  let
    value = "{{># }}"
    isMatch = value.match(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&TOKENS))"), matches)

  doAssert isMatch
  doAssert matches[0] == "{{># }}"


## comment stop token
block:
  var matches: array[4, string]

  let
    value = "{{<# }}"
    isMatch = value.match(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&TOKENS))"), matches)

  doAssert isMatch
  doAssert matches[0] == "{{<# }}"


## if start token
block:
  var matches: array[4, string]

  let
    value = "{{>if 123==$ }}"
    isMatch = value.match(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&TOKENS))"), matches)

  doAssert isMatch
  doAssert matches[0] == "{{>if 123==$ }}"


## if stop token
block:
  var matches: array[4, string]

  let
    value = "{{<if }}"
    isMatch = value.match(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&TOKENS))"), matches)

  doAssert isMatch
  doAssert matches[0] == "{{<if }}"


## elif start token
block:
  var matches: array[4, string]

  let
    value = "{{>elif 123==$ }}"
    isMatch = value.match(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&TOKENS))"), matches)

  doAssert isMatch
  doAssert matches[0] == "{{>elif 123==$ }}"


## else token
block:
  var matches: array[4, string]

  let
    value = "{{>else }}"
    isMatch = value.match(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&TOKENS))"), matches)

  doAssert isMatch
  doAssert matches[0] == "{{>else }}"


## for start token
block:
  var matches: array[4, string]

  let
    value = "{{>for i in $ }}"
    isMatch = value.match(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&TOKENS))"), matches)

  doAssert isMatch
  doAssert matches[0] == "{{>for i in $ }}"


## for stop token
block:
  var matches: array[4, string]

  let
    value = "{{<for }}"
    isMatch = value.match(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&TOKENS))"), matches)

  doAssert isMatch
  doAssert matches[0] == "{{<for }}"
