import std/[re]
import ../../../src/neon/pattern {.all.}


## array
block:
  var matches: array[1, string]

  let
    value = "[1, 2, 3]"
    isMatch = value.match(re("(" & definitions & "(?&ARRAY))"), matches)

  doAssert isMatch
  doAssert matches[0] ==  "[1, 2, 3]"


## nested array
block:
  var matches: array[1, string]

  let
    value = "[1, 2, [1, 2]]"
    isMatch = value.match(re("(" & definitions & "(?&ARRAY))"), matches)

  doAssert isMatch
  doAssert matches[0] ==  "[1, 2, [1, 2]]"


## different types
block:
  var matches: array[1, string]


  let
    value = "[1, {}, [1.2, true]]"
    isMatch = value.match(re("(" & definitions & "(?&ARRAY))"), matches)

  doAssert isMatch
  doAssert matches[0] ==  "[1, {}, [1.2, true]]"
