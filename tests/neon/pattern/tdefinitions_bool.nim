import std/[re]
import ../../../src/neon/pattern {.all.}


## true
block:
  var matches: array[1, string]

  let
    value = "true"
    isMatch = value.match(re("(" & definitions & "(?&BOOL))"), matches)

  doAssert isMatch
  doAssert matches[0] == "true"


## false
block:
  var matches: array[1, string]

  let
    value = "false"
    isMatch = value.match(re("(" & definitions & "(?&BOOL))"), matches)

  doAssert isMatch
  doAssert matches[0] == "false"


## not a boolean
block:
  var matches: array[1, string]

  let
    value = "some"
    isMatch = value.match(re("(" & definitions & "(?&BOOL))"), matches)

  doAssert not isMatch
