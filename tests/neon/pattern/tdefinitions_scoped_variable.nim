import std/[re]
import ../../../src/neon/pattern {.all.}


## variable
block:
  var matches: array[1, string]

  let
    value = "variable"
    isMatch = value.match(re("(" & definitions & "(?&SCOPED_VARIABLE))"), matches)

  doAssert isMatch
  doAssert matches[0] == "variable"


## variable
block:
  var matches: array[1, string]

  let
    value = "variable.sub"
    isMatch = value.match(re("(" & definitions & "(?&SCOPED_VARIABLE))"), matches)

  doAssert isMatch
  doAssert matches[0] == "variable.sub"

## variable with dot
block:
  var matches: array[1, string]

  let
    value = "variable."
    isMatch = value.match(re("^(" & definitions & "(?&SCOPED_VARIABLE))$"), matches)

  doAssert not isMatch


## true
block:
  var matches: array[1, string]

  let
    value = "true"
    isMatch = value.match(re("(" & definitions & "(?&SCOPED_VARIABLE))"), matches)

  doAssert not isMatch


## false
block:
  var matches: array[1, string]

  let
    value = "false"
    isMatch = value.match(re("(" & definitions & "(?&SCOPED_VARIABLE))"), matches)

  doAssert not isMatch


## variable must start with char
block:
  var matches: array[1, string]

  let
    value = "1dsds"
    isMatch = value.match(re("(" & definitions & "(?&SCOPED_VARIABLE))"), matches)

  doAssert not isMatch


## integer
block:
  var matches: array[1, string]

  let
    value = "123"
    isMatch = value.match(re("(" & definitions & "(?&SCOPED_VARIABLE))"), matches)


## float
block:
  var matches: array[1, string]

  let
    value = "123.23"
    isMatch = value.match(re("(" & definitions & "(?&SCOPED_VARIABLE))"), matches)

  doAssert not isMatch


## string
block:
  var matches: array[1, string]

  let
    value = "\"variable\""
    isMatch = value.match(re("(" & definitions & "(?&SCOPED_VARIABLE))"), matches)

  doAssert not isMatch
