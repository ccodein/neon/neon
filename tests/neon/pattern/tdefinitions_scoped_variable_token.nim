import std/[re, strutils]
import ../../../src/neon/pattern {.all.}


## scoped variable token
block:
  var matches: array[5, string]

  let
    value = "{{variable}}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&SCOPED_VARIABLE_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{variable}}"


## scoped variable token with spaces
block:
  var matches: array[1, string]

  let
    value = "{{  variable  }}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&SCOPED_VARIABLE_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{  variable  }}"


## scoped variable token, appended and leading spaces, newline
block:
  var matches: array[1, string]

  let
    value = "  {{variable}}  \n"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&SCOPED_VARIABLE_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{variable}}"
