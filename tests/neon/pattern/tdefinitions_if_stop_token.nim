import std/[re, strutils]
import ../../../src/neon/pattern {.all.}


## if stop token
block:
  var matches: array[1, string]

  let
    value = "{{<if}}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&IF_STOP_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{<if}}"


## if stop token with spaces
block:
  var matches: array[1, string]

  let
    value = "{{ < if }}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&IF_STOP_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{ < if }}"

## if stop token with appended spaces and newline
block:
  var matches: array[1, string]

  let
    value = "  {{<if}}  \n"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&IF_STOP_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "  {{<if}}  \n"


## if stop token with appended und leading spaces, newline
block:
  var matches: array[1, string]

  let
    value = "  {{<if }}  \n"

    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&IF_STOP_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "  {{<if }}  \n"
