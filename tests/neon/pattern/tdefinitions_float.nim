import std/[re]
import ../../../src/neon/pattern {.all.}


## correct float
block:
  var matches: array[1, string]

  let
    value = "123.12"
    isMatch = value.match(re("(" & definitions & "(?&FLOAT))"), matches)

  doAssert isMatch
  doAssert matches[0] == "123.12"


## negative float value
block:
  var matches: array[1, string]

  let
    value = "-123.12"
    isMatch = value.match(re("(" & definitions & "(?&FLOAT))"), matches)

  doAssert isMatch
  doAssert matches[0] == "-123.12"


## exponential float value
block:
  var matches: array[1, string]

  let
    value = "123.12E2"
    isMatch = value.match(re("(" & definitions & "(?&FLOAT))"), matches)

  doAssert isMatch
  doAssert matches[0] == "123.12E2"


## explicitly positive exponential float value
block:
  var matches: array[1, string]

  let
    value = "123.12E+2"
    isMatch = value.match(re("(" & definitions & "(?&FLOAT))"), matches)

  doAssert isMatch
  doAssert matches[0] == "123.12E+2"


## negative exponential float value
block:
  var matches: array[1, string]

  let
    value = "123.12e-2"
    isMatch = value.match(re("(" & definitions & "(?&FLOAT))"), matches)

  doAssert isMatch
  doAssert matches[0] == "123.12e-2"


## float without digits before the decimal point
block:
  var matches: array[1, string]

  let
    value = ".12"
    isMatch = value.match(re("(" & definitions & "(?&FLOAT))"), matches)


  doAssert not isMatch


## float without digits after the decimal point
block:
  var matches: array[1, string]

  let
    value = "123."
    isMatch = value.match(re("(" & definitions & "(?&FLOAT))"), matches)

  doAssert not isMatch
