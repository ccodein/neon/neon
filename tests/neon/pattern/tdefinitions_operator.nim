import std/[re]
import ../../../src/neon/pattern {.all.}


## equal operator
block:
  var matches: array[7, string]

  let
    value = "=="
    isMatch = value.match(re("(" & definitions & "(?&OPERATOR))"), matches)

  doAssert isMatch
  doAssert matches[0] == "=="


## not equal operator
block:
  var matches: array[7, string]

  let
    value = "!="
    isMatch = value.match(re("(" & definitions & "(?&OPERATOR))"), matches)

  doAssert isMatch
  doAssert matches[0] == "!="


## greater than operator
block:
  var matches: array[7, string]

  let
    value = ">"
    isMatch = value.match(re("(" & definitions & "(?&OPERATOR))"), matches)

  doAssert isMatch
  doAssert matches[0] == ">"


## less than operator
block:
  var matches: array[7, string]

  let
    value = "<"
    isMatch = value.match(re("(" & definitions & "(?&OPERATOR))"), matches)

  doAssert isMatch
  doAssert matches[0] == "<"


## greater than or equal operator
block:
  var matches: array[7, string]

  let
    value = ">="
    isMatch = value.match(re("(" & definitions & "(?&OPERATOR))"), matches)

  doAssert isMatch
  doAssert matches[0] == ">="


## less than or equal operator
block:
  var matches: array[7, string]

  let
    value = "<="
    isMatch = value.match(re("(" & definitions & "(?&OPERATOR))"), matches)

  doAssert isMatch
  doAssert matches[0] == "<="


## aot a regular operator
block:
  var matches: array[7, string]

  let
    value = "*"
    isMatch = value.match(re("(" & definitions & "(?&OPERATOR))"), matches)

  doAssert not isMatch
