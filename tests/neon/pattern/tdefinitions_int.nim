import std/[re]
import ../../../src/neon/pattern {.all.}


## correct int
block:
  var matches: array[1, string]

  let
    value = "123"
    isMatch = value.match(re("(" & definitions & "(?&INT))"), matches)

  doAssert isMatch
  doAssert matches[0] == "123"


## negative int value
block:
  var matches: array[1, string]

  let
    value = "-123"
    isMatch = value.match(re("(" & definitions & "(?&INT))"), matches)

  doAssert isMatch
  doAssert matches[0] == "-123"


## explicit positive int value
block:
  var matches: array[1, string]

  let
    value = "+123"
    isMatch = value.match(re("(" & definitions & "(?&INT))"), matches)

  doAssert isMatch
  doAssert matches[0] == "+123"


## zero int value
block:
  var matches: array[1, string]

  let
    value = "0"
    isMatch = value.match(re("(" & definitions & "(?&INT))"), matches)

  doAssert isMatch
  doAssert matches[0] == "0"


## positive zero
block:
  var matches: array[1, string]

  let
    value = "+0"
    isMatch = value.match(re("(" & definitions & "(?&INT))"), matches)

  doAssert not isMatch


## negative zero
block:
  var matches: array[1, string]

  let
    value = "-0"
    isMatch = value.match(re("(" & definitions & "(?&INT))"), matches)

  doAssert not isMatch
