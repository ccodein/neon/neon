import std/[re]
import ../../../src/neon/pattern {.all.}


## expression without spaces
block:
  var matches: array[1, string]

  let
    value = "123==$"
    isMatch = value.match(re("(" & definitions & "(?&IF_EXPRESSION))"), matches)

  doAssert isMatch
  doAssert matches[0] == "123==$"


## expression with spaces
block:
  var matches: array[2, string]

  let
    value = "variable != true"
    isMatch = value.match(re("(" & definitions & "(?&IF_EXPRESSION))"), matches)

  doAssert isMatch
  doAssert matches[0] == "variable != true"


## only boolean
block:
  var matches: array[2, string]

  let
    value = "true"
    isMatch = value.match(re("(" & definitions & "(?&IF_EXPRESSION))"), matches)

  doAssert isMatch
  doAssert matches[0] == "true"


## only int
block:
  var matches: array[2, string]

  let
    value = "123"
    isMatch = value.match(re("(" & definitions & "(?&IF_EXPRESSION))"), matches)

  doAssert isMatch
  doAssert matches[0] == "123"


## only context variable
block:
  var matches: array[2, string]

  let
    value = "$"
    isMatch = value.match(re("(" & definitions & "(?&IF_EXPRESSION))"), matches)

  doAssert isMatch
  doAssert matches[0] == "$"


## only variable
block:
  var matches: array[2, string]

  let
    value = "variable"
    isMatch = value.match(re("(" & definitions & "(?&IF_EXPRESSION))"), matches)

  doAssert isMatch
  doAssert matches[0] == "variable"
