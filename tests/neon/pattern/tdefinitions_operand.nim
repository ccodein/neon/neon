import std/[re]
import ../../../src/neon/pattern {.all.}


## int value
block:
  var matches: array[1, string]

  let
    value = "123"
    isMatch = value.match(re("(" & definitions & "(?&OPERAND))"), matches)

  doAssert isMatch
  doAssert matches[0] == "123"


## boolean value
block:
  var matches: array[1, string]

  let
    value = "true"
    isMatch = value.match(re("(" & definitions & "(?&OPERAND))"), matches)

  doAssert isMatch
  doAssert matches[0] == "true"


## float value
block:
  var matches: array[1, string]

  let
    value = "123.56"
    isMatch = value.match(re("(" & definitions & "(?&OPERAND))"), matches)

  doAssert isMatch
  doAssert matches[0] == "123.56"


## string value
block:
  var matches: array[1, string]

  let
    value = "\"some\""
    isMatch = value.match(re("(" & definitions & "(?&OPERAND))"), matches)

  doAssert isMatch
  doAssert matches[0] == "\"some\""


## variable
block:
  var matches: array[1, string]

  let
    value = "variable"
    isMatch = value.match(re("(" & definitions & "(?&OPERAND))"), matches)

  doAssert isMatch
  doAssert matches[0] == "variable"


## variable
block:
  var matches: array[1, string]

  let
    value = "$"
    isMatch = value.match(re("(" & definitions & "(?&OPERAND))"), matches)

  doAssert isMatch
  doAssert matches[0] == "$"
