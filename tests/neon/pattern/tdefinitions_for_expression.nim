import std/[re]
import ../../../src/neon/pattern {.all.}


## for with variable and context
block:
  var matches: array[1, string]

  let
    # value = "for i in "
    value = "for i in $"
    isMatch = value.match(re("(" & definitions & "(?&FOR_EXPRESSION))"), matches)

  doAssert isMatch
  doAssert matches[0] == "for i in $"


## for with variable and variable
block:
  var matches: array[2, string]

  let
    value = "for i in variable"
    isMatch = value.match(re("(" & definitions & "(?&FOR_EXPRESSION))"), matches)

  doAssert isMatch
  doAssert matches[0] == "for i in variable"


## for with context and variable
block:
  var matches: array[2, string]

  let
    value = "for $ in variable"
    isMatch = value.match(re("(" & definitions & "(?&FOR_EXPRESSION))"), matches)

  doAssert not isMatch


## for array
block:
  var matches: array[2, string]

  let
    value = "for i in [1, 2, 3]"
    isMatch = value.match(re("(" & definitions & "(?&FOR_EXPRESSION))"), matches)

  doAssert isMatch
  doAssert matches[0] == "for i in [1, 2, 3]"
