import std/[re]
import ../../../src/neon/pattern {.all.}


## multiple keys
block:
  var matches: array[1, string]

  let
    value = """
    {
      "keyNestedIntArray": [1, [2, 3]],
      "keyNestedObject": {"keyFloat": 12.34},
      "keyNull": null,
      "keyBool": true,
      "keyString": "some string"
    }
    """
    isMatch = value.match(re("""(\s*""" & definitions & """(?&OBJECT)\s*)"""), matches)

  doAssert isMatch
  doAssert matches[0] == """
    {
      "keyNestedIntArray": [1, [2, 3]],
      "keyNestedObject": {"keyFloat": 12.34},
      "keyNull": null,
      "keyBool": true,
      "keyString": "some string"
    }
    """
