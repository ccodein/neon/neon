import std/[re, strutils]
import ../../../src/neon/pattern {.all.}


## elif start token without spaces
block:
  var matches: array[3, string]

  let
    value = "{{>elif 123==$}}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&ELIF_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{>elif 123==$}}"


## elif start token with spaces
block:
  var matches: array[4, string]

  let
    value = "{{ > elif variable != true }}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&ELIF_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{ > elif variable != true }}"


## elif start token with appended spaces and newline
block:
  var matches: array[3, string]

  let
    value = "{{>elif 123==$}}  \n"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&ELIF_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{>elif 123==$}}  \n"


## elif start token with appended und leading spaces, newline
block:
  var matches: array[1, string]

  let
    value = "  {{>elif [1, 2, 3]}}  \n"

    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&ELIF_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "  {{>elif [1, 2, 3]}}  \n"
