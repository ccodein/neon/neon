import std/[re]
import ../../../src/neon/pattern {.all.}


## context variable
block:
  var matches: array[1, string]

  let
    value = "$"
    isMatch = value.match(re("(" & definitions & "(?&CONTEXT_VARIABLE))"), matches)

  doAssert isMatch
  doAssert matches[0] == "$"


## context variable with sub-variable
block:
  var matches: array[1, string]

  let
    value = "$.sub"
    isMatch = value.match(re("(" & definitions & "(?&CONTEXT_VARIABLE))"), matches)

  doAssert isMatch
  doAssert matches[0] == "$.sub"


## context variable with multiple sub-variables
block:
  var matches: array[1, string]

  let
    value = "$.sub.subsub.subsubsub"
    isMatch = value.match(re("(" & definitions & "(?&CONTEXT_VARIABLE))"), matches)

  doAssert isMatch
  doAssert matches[0] == "$.sub.subsub.subsubsub"


## context variable with dot operator
block:
  var matches: array[1, string]

  let
    value = "$."
    isMatch = value.match(re("(^" & definitions & "(?&CONTEXT_VARIABLE))$"), matches)

  doAssert not isMatch


# integer
block:
  var matches: array[1, string]

  let
    value = "123"
    isMatch = value.match(re("(" & definitions & "(?&CONTEXT_VARIABLE))"), matches)

  doAssert not isMatch


# float
block:
  var matches: array[1, string]

  let
    value = "123.23"
    isMatch = value.match(re("(" & definitions & "(?&CONTEXT_VARIABLE))"), matches)

  doAssert not isMatch


# string
block:
  var matches: array[1, string]

  let
    value = "\"variable\""
    isMatch = value.match(re("(" & definitions & "(?&CONTEXT_VARIABLE))"), matches)

  doAssert not isMatch
