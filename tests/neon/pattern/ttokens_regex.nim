import std/re
import ../../../src/neon/pattern


block:
  var matches: array[1, string]

  let
    token = "{{>#}}"
    isMatch = token.match(initTokensRegex(r"\{\{", r"\}\}").regex, matches)

  doAssert isMatch
  doAssert matches[0] == "{{>#}}"


block:
  var matches: array[1, string]

  let
    token = "[[>#]]"
    isMatch = token.match(initTokensRegex(r"\[\[", r"\]\]").regex, matches)

  doAssert isMatch
  doAssert matches[0] == "[[>#]]"
