import std/[re, strutils]
import ../../../src/neon/pattern {.all.}


## comment start token
block:
  var matches: array[1, string]

  let
    value = "{{>#}}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&COMMENT_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{>#}}"


## comment start token with spaces
block:
  var matches: array[1, string]

  let
    value = "{{ > # }}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&COMMENT_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{ > # }}"


## comment start token appended and leading spaces, newline
block:
  var matches: array[1, string]

  let
    value = "  {{ > # }}  \n"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&COMMENT_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "  {{ > # }}  \n"
