import std/[re]
import ../../../src/neon/pattern {.all.}


## boolean
block:
  var matches: array[1, string]

  let
    value = "true"
    isMatch = value.match(re("(" & definitions & "(?&VALUE))"), matches)

  doAssert isMatch
  doAssert matches[0] == "true"


## null
block:
  var matches: array[1, string]

  let
    value = "null"
    isMatch = value.match(re("(" & definitions & "(?&VALUE))"), matches)

  doAssert isMatch
  doAssert matches[0] == "null"


## int
block:
  var matches: array[1, string]

  let
    value = "123"
    isMatch = value.match(re("(" & definitions & "(?&VALUE))"), matches)

  doAssert isMatch
  doAssert matches[0] == "123"


## float
block:
  var matches: array[1, string]

  let
    value = "123.45"
    isMatch = value.match(re("(" & definitions & "(?&VALUE))"), matches)

  doAssert isMatch
  doAssert matches[0] == "123.45"


## string
block:
  var matches: array[1, string]

  let
    value = "\"some string\""
    isMatch = value.match(re("(" & definitions & "(?&VALUE))"), matches)

  doAssert isMatch
  doAssert matches[0] == "\"some string\""


## object
block:
  var matches: array[1, string]

  let
    value = "{\"key\": 123}"
    isMatch = value.match(re("(" & definitions & "(?&VALUE))"), matches)

  doAssert isMatch
  doAssert matches[0] ==  "{\"key\": 123}"


## array
block:
  var matches: array[1, string]

  let
    value = "[1, 2, 3]"
    isMatch = value.match(re("(" & definitions & "(?&VALUE))"), matches)

  doAssert isMatch
  doAssert matches[0] ==  "[1, 2, 3]"
