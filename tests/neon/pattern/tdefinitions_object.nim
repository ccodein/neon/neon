import std/[re]
import ../../../src/neon/pattern {.all.}


## object without spaces
block:
  var matches: array[1, string]

  let
    value = "{\"key\":123}"
    isMatch = value.match(re("(" & definitions & "(?&OBJECT))"), matches)

  doAssert isMatch
  doAssert matches[0] ==  "{\"key\":123}"


## object with spaces
block:
  var matches: array[1, string]

  let
    value = "{ \"key\" : 123 }"
    isMatch = value.match(re("(" & definitions & "(?&OBJECT))"), matches)

  doAssert isMatch
  doAssert matches[0] ==  "{ \"key\" : 123 }"


## nested object
block:
  var matches: array[1, string]

  let
    value = "{ \"key\" : {\"nestedKey\": 123} }"
    isMatch = value.match(re("(" & definitions & "(?&OBJECT))"), matches)

  doAssert isMatch
  doAssert matches[0] ==  "{ \"key\" : {\"nestedKey\": 123} }"


## multiple keys
block:
  var matches: array[1, string]

  let
    value = "{\"keyOne\": 123, \"keyTwo\": 12.34}"
    isMatch = value.match(re("(" & definitions & "(?&OBJECT))"), matches)

  doAssert isMatch
  doAssert matches[0] == "{\"keyOne\": 123, \"keyTwo\": 12.34}"

