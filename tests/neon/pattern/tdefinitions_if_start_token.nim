import std/[re, strutils]
import ../../../src/neon/pattern {.all.}


## if start token without spaces
block:
  var matches: array[1, string]

  let
    value = "{{>if 123==$}}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&IF_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{>if 123==$}}"


## if start token without appended spaces and newline
block:
  var matches: array[1, string]

  let
    value = "{{>if 123==$}}  \n"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&IF_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{>if 123==$}}  \n"


## if start token with spaces
block:
  var matches: array[1, string]

  let
    value = "{{ > if variable != true }}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&IF_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{ > if variable != true }}"


## if start token with variable and without spaces
block:
  var matches: array[1, string]

  let
    value = "{{>if variable}}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&IF_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{>if variable}}"


## if start token with variable and spaces
block:
  var matches: array[1, string]

  let
    value = "{{ > if variable }}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&IF_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{ > if variable }}"


## if start token with context variable
block:
  var matches: array[1, string]

  let
    value = "{{>if $}}"
    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&IF_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "{{>if $}}"


## if start token with appended und leading spaces, newline
block:
  var matches: array[1, string]

  let
    value = "  {{>if [1, 2, [3, 4]]}}  \n"

    bounds = value.findBounds(re(
      "(" & definitions.replace("_BEGIN_", r"\{\{").replace("_END_", r"\}\}") &
      "(?&IF_START_TOKEN))"), matches)

  doAssert bounds.first != -1
  doAssert matches[0] == "  {{>if [1, 2, [3, 4]]}}  \n"
