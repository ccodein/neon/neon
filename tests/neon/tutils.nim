import std/[json]
import ../../src/neon/utils
import ../../src/neon/exceptions


## the key exists
block:
  let data = %* {"key": "value"}

  doAssert data.valueByDotNotation("key") == %* "value"


## the key does not exists
block:
  let data = %* {"key": "value"}

  doAssertRaises(KeyNotExistsError):
    discard data.valueByDotNotation("wrongKey")


## with nested key
block:
  let data = %* {"key": {"nestedKey": "value"}}

  doAssert data.valueByDotNotation("key.nestedKey") == %* "value"


## the key is not an object
block:
  let data = %* {"key": 123}

  doAssertRaises(NotAnObjectError):
    discard data.valueByDotNotation("key.nestedKey")
