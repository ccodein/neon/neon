import std/[strutils]
import ../../src/neon/templates


block:
  let
    data = """
    <div>
      {{ $ }}

      {{ ># }}
        some comment
      {{ <# }}

      {{ >if 123 == $}}
        body
        {{ >if 123 == $}}
          inner
        {{ <if }}
      {{ <if }}
    </div>
    """.dedent
  var tmpl = initTemplate(data)

  doAssert $tmpl == """
    kind: nkRoot
    nodes:
    ------  
      
      index: 0
      kind: nkContextVariable
      name: $
      start token:
        kind: tkContextVariable
        match: "{{ $ }}"
        position: 8 .. 14  
      
      index: 1
      kind: nkComment
      start token:
        kind: tkCommentStart
        match: "  {{ ># }}\x0A"
        position: 17 .. 27
      stop token:
        kind: tkCommentStop
        match: "  {{ <# }}\x0A"
        position: 45 .. 55  
      
      index: 2
      kind: nkIf
      node count: 1
      expression: 123==$
      start token:
        kind: tkIfStart
        match: "  {{ >if 123 == $}}\x0A"
        position: 57 .. 76
      stop token:
        kind: tkIfStop
        match: "  {{ <if }}\x0A"
        position: 134 .. 145  
      nodes:
      ------    
        
        index: 0
        kind: nkIf
        node count: 0
        expression: 123==$
        start token:
          kind: tkIfStart
          match: "    {{ >if 123 == $}}\x0A"
          position: 86 .. 107
        stop token:
          kind: tkIfStop
          match: "    {{ <if }}\x0A"
          position: 120 .. 133""".dedent
