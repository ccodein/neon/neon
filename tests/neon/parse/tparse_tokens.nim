import std/[strutils, json]
import ../../../src/neon/parse
import ../../../src/neon/token
import ../../../src/neon/pattern
import ../../../src/neon/exceptions


# root node
block:
  let
    tmpl = """
    <div> </div>
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.parent == nil
  doAssert rootNode.startToken == nil
  doAssert rootNode.stopToken == nil
  doAssert rootNode.nodes.len == 0


## context node
block:
  let
    tmpl = """
    {{ $ }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes.len == 1
  doAssert rootNode.nodes[0].kind == nkContextVariable
  doAssert rootNode.nodes[0].name == "$"
  doAssert rootNode.nodes[0].parent == rootNode
  doAssert rootNode.nodes[0].startToken == tokens[0]


## multiple context nodes
block:
  let
    tmpl = """
    {{ $ }}
    {{ $.content }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes.len == 2
  doAssert rootNode.nodes[0].kind == nkContextVariable
  doAssert rootNode.nodes[0].name == "$"
  doAssert rootNode.nodes[0].parent == rootNode
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[1].kind == nkContextVariable
  doAssert rootNode.nodes[1].name == "$.content"
  doAssert rootNode.nodes[1].parent == rootNode
  doAssert rootNode.nodes[1].startToken == tokens[1]


## multiple context tokens in one line
block:
  let
    tmpl = """
    {{ $ }} {{ $.content }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes.len == 2
  doAssert rootNode.nodes[0].kind == nkContextVariable
  doAssert rootNode.nodes[0].name == "$"
  doAssert rootNode.nodes[0].parent == rootNode
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[1].kind == nkContextVariable
  doAssert rootNode.nodes[1].name == "$.content"
  doAssert rootNode.nodes[1].parent == rootNode
  doAssert rootNode.nodes[1].startToken == tokens[1]


## variable node
block:
  let
    tmpl = """
    {{ variable }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes.len == 1
  doAssert rootNode.nodes[0].kind == nkScopedVariable
  doAssert rootNode.nodes[0].name == "variable"
  doAssert rootNode.nodes[0].parent == rootNode
  doAssert rootNode.nodes[0].startToken == tokens[0]


# multiple variable nodes
block:
  let
    tmpl = """
    {{ one }}
    {{ two }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes.len == 2
  doAssert rootNode.nodes[0].kind == nkScopedVariable
  doAssert rootNode.nodes[0].name == "one"
  doAssert rootNode.nodes[0].parent == rootNode
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[1].kind == nkScopedVariable
  doAssert rootNode.nodes[1].name == "two"
  doAssert rootNode.nodes[1].parent == rootNode
  doAssert rootNode.nodes[1].startToken == tokens[1]


# multiple scoped variable tokens in one line
block:
  let
    tmpl = """
    {{ one }} {{ two }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes.len == 2
  doAssert rootNode.nodes[0].kind == nkScopedVariable
  doAssert rootNode.nodes[0].name == "one"
  doAssert rootNode.nodes[0].parent == rootNode
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[1].kind == nkScopedVariable
  doAssert rootNode.nodes[1].name == "two"
  doAssert rootNode.nodes[1].parent == rootNode
  doAssert rootNode.nodes[1].startToken == tokens[1]


# comment with inner token
block:
  let
    tmpl = """
    {{ variableOne }}

    {{># }}
      some comment
      {{ variableTwo }}
    {{<# }}

    {{ variableThree }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes.len == 3
  doAssert rootNode.nodes[0].kind == nkScopedVariable
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[1].nodes.len == 0
  doAssert rootNode.nodes[1].kind == nkComment
  doAssert rootNode.nodes[1].startToken == tokens[1]
  doAssert rootNode.nodes[1].stopToken == tokens[3]
  doAssert rootNode.nodes[2].kind == nkScopedVariable
  doAssert rootNode.nodes[2].startToken == tokens[4]


# comment with inner token
block:
  let
    tmpl = """
    {{ variableOne }}

    {{># }} some comment {{<# }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes.len == 2
  doAssert rootNode.nodes[0].kind == nkScopedVariable
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[1].nodes.len == 0
  doAssert rootNode.nodes[1].kind == nkComment
  doAssert rootNode.nodes[1].startToken == tokens[1]
  doAssert rootNode.nodes[1].stopToken == tokens[2]


## comment without body
block:
  let
    tmpl = """
    {{># }}{{<# }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(TokenHasNoBodyError):
    discard tokens.parseTokens(tmpl)


## nested comment
block:
  let
    tmpl = """
    {{ variableOne }}

    {{>if $ }}
      {{># }}
        some comment
      {{<# }}
    {{<if }}

    {{ variableThree }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes.len == 3
  doAssert rootNode.nodes[0].kind == nkScopedVariable
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[1].kind == nkIf
  doAssert rootNode.nodes[1].startToken == tokens[1]
  doAssert rootNode.nodes[1].stopToken == tokens[4]
  doAssert rootNode.nodes[1].nodes.len == 1
  doAssert rootNode.nodes[1].nodes[0].nodes.len == 0
  doAssert rootNode.nodes[1].nodes[0].kind == nkComment
  doAssert rootNode.nodes[1].nodes[0].startToken == tokens[2]
  doAssert rootNode.nodes[1].nodes[0].stopToken == tokens[3]
  doAssert rootNode.nodes[2].kind == nkScopedVariable
  doAssert rootNode.nodes[2].startToken == tokens[5]


## comment not closed
block:
  let
    tmpl = """
      {{># }}
        some comment
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(TokenNotClosedError):
    discard tokens.parseTokens(tmpl)


## comment not closed
block:
  let
    tmpl = """
    {{>if $ }}
        some comment
      {{<# }}
    {{<if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(TokenNotOpenedError):
    discard tokens.parseTokens(tmpl)

## if nodes
block:
  let
    tmpl = """
    {{ >if $==123 }}
      body
    {{ <if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes.len == 1
  doAssert rootNode.nodes[0].kind == nkIf
  doAssert rootNode.nodes[0].expression.leftOperand.kind == odkContextVariable
  doAssert rootNode.nodes[0].expression.leftOperand.name == "$"
  doAssert rootNode.nodes[0].expression.rightOperand.kind == odkValue
  doAssert rootNode.nodes[0].expression.rightOperand.value.kind == JInt
  doAssert rootNode.nodes[0].expression.rightOperand.value.num == 123
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[0].stopToken == tokens[1]


## if node without body
block:
  let
    tmpl = """
    {{ >if $==123 }}{{ <if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(TokenHasNoBodyError):
    discard tokens.parseTokens(tmpl)


## if node without body with elif
block:
  let
    tmpl = """
    {{ >if $==123 }}
    {{ >elif $}}
      elif body
    {{ <if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(TokenHasNoBodyError):
    discard tokens.parseTokens(tmpl)


## if node without body with else
block:
  let
    tmpl = """
    {{ >if $==123 }}
    {{ >else}}
      elif body
    {{ <if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(TokenHasNoBodyError):
    discard tokens.parseTokens(tmpl)


## if node with multiple elif nodes, first without body
block:
  let
    tmpl = """
    {{ >if $==123 }}
      body
    {{ >elif $ }}
    {{ >elif $ }}
      body
    {{ <if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(TokenHasNoBodyError):
    discard tokens.parseTokens(tmpl)


## if node with multiple elif nodes, second without body
block:
  let
    tmpl = """
    {{ >if $==123 }}
      body
    {{ >elif $ }}
      body
    {{ >elif $ }}
    {{ <if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(TokenHasNoBodyError):
    discard tokens.parseTokens(tmpl)


## if node with elif and else, elif without body
block:
  let
    tmpl = """
    {{ >if $==123 }}
      body
    {{ >elif $ }}
    {{ >else }}
      body
    {{ <if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(TokenHasNoBodyError):
    discard tokens.parseTokens(tmpl)


## multiple if nodes
block:
  let
    tmpl = """
    {{ >if $ }}
      body one
    {{ <if }}

    {{ >if 123==variable }}
      body two
    {{ <if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes.len == 2
  doAssert rootNode.nodes[0].kind == nkIf
  doAssert rootNode.nodes[0].expression.leftOperand.kind == odkContextVariable
  doAssert rootNode.nodes[0].expression.leftOperand.name == "$"
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[0].stopToken == tokens[1]
  doAssert rootNode.nodes[1].kind == nkIf
  doAssert rootNode.nodes[1].expression.leftOperand.kind == odkValue
  doAssert rootNode.nodes[1].expression.leftOperand.value.kind == JInt
  doAssert rootNode.nodes[1].expression.leftOperand.value.num == 123
  doAssert rootNode.nodes[1].expression.rightOperand.kind == odkScopedVariable
  doAssert rootNode.nodes[1].expression.rightOperand.name == "variable"
  doAssert rootNode.nodes[1].startToken == tokens[2]
  doAssert rootNode.nodes[1].stopToken == tokens[3]


## nested if
block:
  let
    tmpl = """
    {{>if $ }}
      body one
      {{>if variable }}
        body two
      {{<if }}
    {{<if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes.len == 1
  doAssert rootNode.nodes[0].kind == nkIf
  doAssert rootNode.nodes[0].expression.leftOperand.kind == odkContextVariable
  doAssert rootNode.nodes[0].expression.leftOperand.name == "$"
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[0].stopToken == tokens[3]
  doAssert rootNode.nodes[0].nodes.len == 1
  doAssert rootNode.nodes[0].nodes[0].expression.leftOperand.kind == odkScopedVariable
  doAssert rootNode.nodes[0].nodes[0].expression.leftOperand.name == "variable"
  doAssert rootNode.nodes[0].nodes[0].startToken == tokens[1]
  doAssert rootNode.nodes[0].nodes[0].stopToken == tokens[2]


## if with elif
block:
  let
    tmpl = """
    {{>if $ }}
      if body
    {{>elif $ }}
      elif body
    {{<if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes[0].nodes.len == 1
  doAssert rootNode.nodes[0].kind == nkIf
  doAssert rootNode.nodes[0].expression.leftOperand.kind == odkContextVariable
  doAssert rootNode.nodes[0].expression.leftOperand.name == "$"
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[0].stopToken == tokens[2]
  doAssert rootNode.nodes[0].nodes[0].kind == nkElif
  doAssert rootNode.nodes[0].nodes[0].expression.leftOperand.kind == odkContextVariable
  doAssert rootNode.nodes[0].nodes[0].expression.leftOperand.name == "$"
  doAssert rootNode.nodes[0].nodes[0].startToken == tokens[1]
  doAssert rootNode.nodes[0].nodes[0].stopToken == tokens[2]


## if with elif
block:
  let
    tmpl = """
    {{>if $ }}
      if body
    {{>elif $ }}
      elif body
    {{<if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes[0].nodes.len == 1
  doAssert rootNode.nodes[0].kind == nkIf
  doAssert rootNode.nodes[0].expression.leftOperand.kind == odkContextVariable
  doAssert rootNode.nodes[0].expression.leftOperand.name == "$"
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[0].stopToken == tokens[2]
  doAssert rootNode.nodes[0].nodes[0].kind == nkElif
  doAssert rootNode.nodes[0].nodes[0].expression.leftOperand.kind == odkContextVariable
  doAssert rootNode.nodes[0].nodes[0].expression.leftOperand.name == "$"
  doAssert rootNode.nodes[0].nodes[0].startToken == tokens[1]
  doAssert rootNode.nodes[0].nodes[0].stopToken == tokens[2]


## if with elif and no else body
block:
  let
    tmpl = """
    {{>if $ }}
      if body
    {{>elif $ }}
    {{<if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(TokenHasNoBodyError):
    discard tokens.parseTokens(tmpl)


## if with else and no else body
block:
  let
    tmpl = """
    {{>if $ }}
      if body
    {{>else }}
    {{<if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(TokenHasNoBodyError):
    discard tokens.parseTokens(tmpl)


## if with elif and else
block:
  let
    tmpl = """
    {{>if $ }}
      if body
    {{>elif $ }}
      elif body
    {{>else }}
      else body
    {{<if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes[0].nodes.len == 2
  doAssert rootNode.nodes[0].kind == nkIf
  doAssert rootNode.nodes[0].expression.leftOperand.kind == odkContextVariable
  doAssert rootNode.nodes[0].expression.leftOperand.name == "$"
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[0].stopToken == tokens[3]
  doAssert rootNode.nodes[0].nodes[0].kind == nkElif
  doAssert rootNode.nodes[0].nodes[0].startToken == tokens[1]
  doAssert rootNode.nodes[0].nodes[0].stopToken == tokens[2]
  doAssert rootNode.nodes[0].nodes[1].kind == nkElse
  doAssert rootNode.nodes[0].nodes[1].startToken == tokens[2]
  doAssert rootNode.nodes[0].nodes[1].stopToken == tokens[3]


## if with elif and else inline
block:
  let
    tmpl = """
    {{>if $ }} if body {{>elif $ }} elif body {{>else }} else body {{<if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes[0].nodes.len == 2
  doAssert rootNode.nodes[0].kind == nkIf
  doAssert rootNode.nodes[0].expression.leftOperand.kind == odkContextVariable
  doAssert rootNode.nodes[0].expression.leftOperand.name == "$"
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[0].stopToken == tokens[3]
  doAssert rootNode.nodes[0].nodes[0].kind == nkElif
  doAssert rootNode.nodes[0].nodes[0].startToken == tokens[1]
  doAssert rootNode.nodes[0].nodes[0].stopToken == tokens[2]
  doAssert rootNode.nodes[0].nodes[1].kind == nkElse
  doAssert rootNode.nodes[0].nodes[1].startToken == tokens[2]
  doAssert rootNode.nodes[0].nodes[1].stopToken == tokens[3]


## if with elif and else
block:
  let
    tmpl = """
    {{>if $ }}
      if body
    {{>elif $ }}
      elif body one
    {{>elif $ }}
      elif body two
    {{<if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes[0].nodes.len == 2
  doAssert rootNode.nodes[0].kind == nkIf
  doAssert rootNode.nodes[0].expression.leftOperand.kind == odkContextVariable
  doAssert rootNode.nodes[0].expression.leftOperand.name == "$"
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[0].stopToken == tokens[3]
  doAssert rootNode.nodes[0].nodes[0].kind == nkElif
  doAssert rootNode.nodes[0].nodes[0].startToken == tokens[1]
  doAssert rootNode.nodes[0].nodes[0].stopToken == tokens[2]
  doAssert rootNode.nodes[0].nodes[1].kind == nkElif
  doAssert rootNode.nodes[0].nodes[1].startToken == tokens[2]
  doAssert rootNode.nodes[0].nodes[1].stopToken == tokens[3]


## if with multiple elif and else
block:
  let
    tmpl = """
    {{>if $ }}
      if body
    {{>elif $ }}
      elif body one
    {{>elif $ }}
      elif body two
    {{>else }}
      else body
    {{<if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes[0].nodes.len == 3
  doAssert rootNode.nodes[0].kind == nkIf
  doAssert rootNode.nodes[0].expression.leftOperand.kind == odkContextVariable
  doAssert rootNode.nodes[0].expression.leftOperand.name == "$"
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[0].stopToken == tokens[4]
  doAssert rootNode.nodes[0].nodes[0].kind == nkElif
  doAssert rootNode.nodes[0].nodes[0].startToken == tokens[1]
  doAssert rootNode.nodes[0].nodes[0].stopToken == tokens[2]
  doAssert rootNode.nodes[0].nodes[1].kind == nkElif
  doAssert rootNode.nodes[0].nodes[1].startToken == tokens[2]
  doAssert rootNode.nodes[0].nodes[1].stopToken == tokens[3]
  doAssert rootNode.nodes[0].nodes[2].kind == nkElse
  doAssert rootNode.nodes[0].nodes[2].startToken == tokens[3]
  doAssert rootNode.nodes[0].nodes[2].stopToken == tokens[4]


## elif token not inside if tokens
block:
  let
    tmpl = """
    {{>elif $ }}
      body
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(ParentTokenError):
    discard tokens.parseTokens(tmpl)


## else token not inside if tokens
block:
  let
    tmpl = """
    {{>else }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(ParentTokenError):
    discard tokens.parseTokens(tmpl)


## if not closed
block:
  let
    tmpl = """
    {{>if $ }}
      body
    {{<if }}
    {{>if $ }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(TokenNotClosedError):
    discard tokens.parseTokens(tmpl)

## if not opened
block:
  let
    tmpl = """
    {{<if }}
    {{>if $ }}
      body
    {{<if }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(TokenNotOpenedError):
    discard tokens.parseTokens(tmpl)


## for loop
block:
  let
    tmpl = """
    {{ > for i in $ }}
      some
    {{<for }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes.len == 1
  doAssert rootNode.nodes[0].nodes.len == 0
  doAssert rootNode.nodes[0].kind == nkFor
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[0].stopToken == tokens[1]
  doAssert rootNode.nodes[0].variables.variable == "i"
  doAssert rootNode.nodes[0].variables.iterable.kind == ikContextVariable
  doAssert rootNode.nodes[0].variables.iterable.name == "$"


## for loop inline
block:
  let
    tmpl = """
    {{ > for i in $ }} some {{<for }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes.len == 1
  doAssert rootNode.nodes[0].nodes.len == 0
  doAssert rootNode.nodes[0].kind == nkFor
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[0].stopToken == tokens[1]
  doAssert rootNode.nodes[0].variables.variable == "i"
  doAssert rootNode.nodes[0].variables.iterable.kind == ikContextVariable
  doAssert rootNode.nodes[0].variables.iterable.name == "$"


## for loop without body
block:
  let
    tmpl = """
    {{ > for i in $ }}
    {{<for }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
  
  doAssertRaises(TokenHasNoBodyError):
    discard  tokens.parseTokens(tmpl)


# for loop with for loop
block:
  let
    tmpl = """
    {{>for i in $ }}
      body one

      {{>for n in variable }}
        body two
      {{<for }}
    {{<for }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert rootNode.nodes.len == 1
  doAssert rootNode.nodes[0].kind == nkFor
  doAssert rootNode.nodes[0].startToken == tokens[0]
  doAssert rootNode.nodes[0].stopToken == tokens[3]
  doAssert rootNode.nodes[0].variables.variable == "i"
  doAssert rootNode.nodes[0].variables.iterable.kind == ikContextVariable
  doAssert rootNode.nodes[0].variables.iterable.name == "$"
  doAssert rootNode.nodes[0].nodes.len == 1
  doAssert rootNode.nodes[0].nodes[0].kind == nkFor
  doAssert rootNode.nodes[0].nodes[0].startToken == tokens[1]
  doAssert rootNode.nodes[0].nodes[0].stopToken == tokens[2]
  doAssert rootNode.nodes[0].nodes[0].variables.variable == "n"
  doAssert rootNode.nodes[0].nodes[0].variables.iterable.kind == ikScopedVariable
  doAssert rootNode.nodes[0].nodes[0].variables.iterable.name == "variable"


## for not opened
block:
  let
    tmpl = """
    {{>for i in $ }}
        {{>if $}}
          body
        {{<if }}
      {{<for }}
    {{<for }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(TokenNotOpenedError):
    discard tokens.parseTokens(tmpl)


## for wrong opening tag format
block:
  let
    tmpl = """
    {{>for i in $ }}
      {{>for 123 in $ }}
      {{<for }}
    {{<for }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(TokenNotOpenedError):
    discard tokens.parseTokens(tmpl)


## for opening tag not closed
block:
  let
    tmpl = """
    {{>for i in $ }}
      {{>for i in var }}
        body
    {{<for }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)

  doAssertRaises(TokenNotClosedError):
    discard tokens.parseTokens(tmpl)
