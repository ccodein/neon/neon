import std/json
import ../../../src/neon/expression
import ../../../src/neon/exceptions
import ../../../src/neon/parse {.all.}


## parse variable and int value
block:
  let
    expressionString = "variable==123"
    expression = expressionString.parseExpression()


  doAssert expression.leftOperand.kind == odkScopedVariable
  doAssert expression.leftOperand.name == "variable"
  doAssert expression.rightOperand.kind == odkValue
  doAssert expression.rightOperand.value.kind == JInt
  doAssert expression.rightOperand.value.num == 123
  doAssert expression.operator == otkEqual


## parse context and float value
block:
  let
    expressionString = "$>=123.34"
    expression = expressionString.parseExpression()

  doAssert expression.leftOperand.kind == odkContextVariable
  doAssert expression.leftOperand.name == "$"
  doAssert expression.rightOperand.kind == odkValue
  doAssert expression.rightOperand.value.kind == JFloat
  doAssert expression.rightOperand.value.fnum == 123.34
  doAssert expression.operator == otkGreaterThanEqual


## parse boolean value and variable
block:
  let 
    expressionString = "true!=variable"
    expression = expressionString.parseExpression()

  doAssert expression.leftOperand.kind == odkValue
  doAssert expression.leftOperand.value.kind == JBool
  doAssert expression.leftOperand.value.bval == true
  doAssert expression.rightOperand.kind == odkScopedVariable
  doAssert expression.rightOperand.name == "variable"
  doAssert expression.operator == otkNotEqual


## parse context und bool value
block:
  let
    expressionString = "$<false"
    expression = expressionString.parseExpression()

  doAssert expression.leftOperand.kind == odkContextVariable
  doAssert expression.leftOperand.name == "$"
  doAssert expression.rightOperand.kind == odkValue
  doAssert expression.rightOperand.value.kind == JBool
  doAssert expression.rightOperand.value.bval == false
  doAssert expression.operator == otkLessThan


## two values
block:
  let
    expressionString = "123<345"
    expression = expressionString.parseExpression()

  doAssert expression.leftOperand.kind == odkValue
  doAssert expression.leftOperand.value.kind == JInt
  doAssert expression.leftOperand.value.num == 123
  doAssert expression.rightOperand.kind == odkValue
  doAssert expression.rightOperand.value.kind == JInt
  doAssert expression.rightOperand.value.num == 345
  doAssert expression.operator == otkLessThan


## only left operand of variable
block:
  let
    expressionString = "variable"
    expression = expressionString.parseExpression()

  doAssert expression.leftOperand.kind == odkScopedVariable
  doAssert expression.leftOperand.name == "variable"
  doAssert expression.operator == otkNone


## only left operand of context
block:
  let
    expressionString = "$"
    expression = expressionString.parseExpression()

  doAssert expression.leftOperand.kind == odkContextVariable
  doAssert expression.leftOperand.name == "$"
  doAssert expression.operator == otkNone


## one operand of Json
block:
  let
    expressionString = "123"
    expression = expressionString.parseExpression()

  doAssert expression.leftOperand.kind == odkValue
  doAssert expression.leftOperand.value.kind == JInt
  doAssert expression.leftOperand.value.num == 123


## left operand muss be a variable, a context or Json
block:
  let
    expressionString = "#sbc==123"

  doAssertRaises(ExpressionError):
    discard expressionString.parseExpression()


## right operand muss be a variable, a context or Json
block:
  let
    expressionString = "#1123==#sbc"

  doAssertRaises(ExpressionError):
    discard expressionString.parseExpression()
