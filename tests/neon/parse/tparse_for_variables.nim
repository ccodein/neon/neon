import std/json
import ../../../src/neon/parse {.all.}
import ../../../src/neon/exceptions


## parse for with context iterable
block:
  let
    value = "for i in $"
    forVariables = value.parseForVariables()

  doAssert forVariables.variable == "i"
  doAssert forVariables.iterable.kind == ikContextVariable
  doAssert forVariables.iterable.name == "$"


## parse for with context iterable
block:
  let
    value = "for i in variable"
    forVariables = value.parseForVariables()

  doAssert forVariables.variable == "i"
  doAssert forVariables.iterable.kind == ikScopedVariable
  doAssert forVariables.iterable.name == "variable"


## parse for with context iterable
block:
  let
    value = "for i in [1, 2, 3]"
    forVariables = value.parseForVariables()

  doAssert forVariables.variable == "i"
  doAssert forVariables.iterable.kind == ikValue
  doAssert forVariables.iterable.value.kind == JArray
  doAssert $forVariables.iterable.value.elems == "@[1, 2, 3]"


## parse for with context iterable
block:
  let value = "for i in {"

  doAssertRaises(TypeError):
    discard value.parseForVariables()
