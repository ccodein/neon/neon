import std/[json]
import ../../src/neon/stack
import ../../src/neon/exceptions


## variable was added to the stack
block:
  let
    stack = Stack()

  stack.push("a", %* "value")

  doAssert stack.value("a") == %* "value"


## last added variable of same name was returned
block:
  let
    stack = Stack()

  stack.push("a", %* "first value")
  stack.push("a", %* "second value")

  doAssert stack.value("a") == %* "second value"


## last added variable of same name was removed
block:
  let
    stack = Stack()

  stack.push("a", %* "first value")
  stack.push("a", %* "second value")
  stack.pop("a")

  doAssert stack.value("a") == %* "first value"


## last added variable of same name was removed
block:
  let
    stack = Stack()

  stack.push("a", %* "first value")
  stack.push("a", %* "second value")
  stack.update("a", %* "second value updated")

  doAssert stack.value("a") == %* "second value updated"


## different variable names
block:
  let
    stack = Stack()

  stack.push("a", %* "a value")
  stack.push("b", %* "b value")
  stack.update("a", %* "a value updated")
  stack.update("b", %* "b value updated")

  doAssert stack.value("a") == %* "a value updated"
  doAssert stack.value("b") == %* "b value updated"


## sub-key does not exist
block:
  let
    stack = Stack()

  stack.push("a", %* 123)


  doAssertRaises(StackError):
    discard stack.value("a.key")


## value of sub-key of the variable
block:
  let
    stack = Stack()

  stack.push("a", %* {"key": "value"})

  doAssert stack.value("a.key") == %* "value"


## get a non-existent variable
block:
  let
    stack = Stack()

  doAssertRaises(StackError):
    discard stack.value("a")


## update a non-existent variable
block:
  let
    stack = Stack()

  doAssertRaises(StackError):
    stack.update("a", %* "update")


## remove a non-existent variable
block:
  let
    stack = Stack()

  doAssertRaises(StackError):
    stack.pop("a")
