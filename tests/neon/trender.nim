import std/[strutils]
import ../../src/neon/render
import ../../src/neon/templates
import ../../src/neon/exceptions


## no tokens in template
block:
  let
    tmplString = """
    <div>
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* 123

  doAssert tmpl.render(context) == """
    <div>
    </div>
    """.dedent


## context variable
block:
  let
    tmplString = """
    <div>
      {{ $ }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* 123

  doAssert tmpl.render(context) == """
    <div>
      123
    </div>
    """.dedent


# multiple context variables
block:
  let
    tmplString = """
    <div>
      {{ $.header }}
      {{ $.content }}
      {{ $.footer }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* {"header": "header", "content": "content", "footer": "footer"}

  doAssert tmpl.render(context) == """
    <div>
      header
      content
      footer
    </div>
    """.dedent


## scoped variable not on the stack
block:
  let
    tmplString = """
    <div>
      {{ variable }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* nil

  doAssertRaises(StackError):
    discard tmpl.render(context)


## for loop with scoped variable
block:
  let
    tmplString = """
    <div>
      {{>for i in [1, 2, 3] }}
        {{ i }}
      {{<for }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* 123

  doAssert tmpl.render(context) == """
    <div>
        1
        2
        3
    </div>
    """.dedent


## nested for loop
block:
  let
    tmplString = """
    <div>
      {{>for i in [1, 2, 3] }}
        {{>for n in [4, 5, 6] }}
          {{ i }} {{ n }}
        {{<for }}
      {{<for }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* 123

  doAssert tmpl.render(context) == """
    <div>
          1 4
          1 5
          1 6
          2 4
          2 5
          2 6
          3 4
          3 5
          3 6
    </div>
    """.dedent


## for loop with scopes and context variable
block:
  let
    tmplString = """
    <div>
      {{>for i in [1, 2, 3] }}
        {{ i }} {{ $ }}
      {{<for }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* 123

  doAssert tmpl.render(context) == """
    <div>
        1 123
        2 123
        3 123
    </div>
    """.dedent


## for loop with iterable from context variable
block:
  let
    tmplString = """
    <div>
      {{>for i in $ }}
        {{ i }}
      {{<for }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* [1, 2, 3]

  doAssert tmpl.render(context) == """
    <div>
        1
        2
        3
    </div>
    """.dedent

## for loop with iterable from context variable
block:
  let
    tmplString = """
    <div>
      {{>for i in 123 }}
        {{ i }}
      {{<for }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* [1, 2, 3]

  doAssertRaises(TypeError):
    discard tmpl.render(context)


## for loop with inner loop with scoped variable
block:
  let
    tmplString = """
    <div>
      {{>for i in [[1, 2], [3, 4]] }}
        body
        {{>for n in i }}
          {{ n }}
        {{<for }}
      {{<for }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* nil

  doAssert tmpl.render(context) == """
    <div>
        body
          1
          2
        body
          3
          4
    </div>
    """.dedent


## for loop with context variable and inner loop with scoped variable
block:
  let
    tmplString = """
    <div>
      {{>for i in $ }}
        body
        {{>for n in i }}
          {{ n }}
        {{<for }}
      {{<for }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* [[1, 2], [3, 4]]

  doAssert tmpl.render(context) == """
    <div>
        body
          1
          2
        body
          3
          4
    </div>
    """.dedent

## for loop with comment
block:
  let
    tmplString = """
    <div>
      {{>for i in [1, 2, 3] }}
        {{ i }}
        {{>#}}
          some comment
        {{<# }}
      {{<for }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* nil

  doAssert tmpl.render(context) == """
    <div>
        1
        2
        3
    </div>
    """.dedent


## if tag
block:
  let
    tmplString = """
    <div>
      {{>if true}}
          if body
      {{<if }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* nil

  doAssert tmpl.render(context) == """
    <div>
          if body
    </div>
    """.dedent


## if tag with context variable in body
block:
  let
    tmplString = """
    <div>
      {{>if true}}
          {{ $.body }}
      {{<if }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* {"body": "if body"}

  doAssert tmpl.render(context) == """
    <div>
          if body
    </div>
    """.dedent


## comment before if
block:
  let
    tmplString = """
    <div>
      {{ $ }}

      {{ ># }}
        some comment
      {{ <# }}

      {{ >if 123 == $}}
        body
      {{ <if }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* 123

  doAssert tmpl.render(context) == """
    <div>
      123


        body
    </div>
    """.dedent


## if tag with elif and else, if is true
block:
  let
    tmplString = """
    <div>
      {{>if true}}
        if body
      {{>elif true }}
        elif body
      {{>else }}
        eles body
      {{<if }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* true

  doAssert tmpl.render(context) == """
    <div>
        if body
    </div>
    """.dedent


## if tag with elif and else, elif is true
block:
  let
    tmplString = """
    <div>
      {{>if false}}
        if body
      {{>elif true }}
        elif body
      {{>else }}
        eles body
      {{<if }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* true

  doAssert tmpl.render(context) == """
    <div>
        elif body
    </div>
    """.dedent


## if tag with elif and else, elif and if are false
block:
  let
    tmplString = """
    <div>
      {{>if false}}
        if body
      {{>elif false }}
        elif body
      {{>else }}
        else body
      {{<if }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* true

  doAssert tmpl.render(context) == """
    <div>
        else body
    </div>
    """.dedent


## for tag with if
block:
  let
    tmplString = """
    <div>
      {{>for i in [true, false, true, false] }}
        {{>if i}}
          {{ i }}
        {{<if }}
      {{<for }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* true

  doAssert tmpl.render(context) == """
    <div>
          true
          true
    </div>
    """.dedent


## for loop with array of objects
block:
  let
    tmplString = """
    <div>
      {{>for i in $ }}
        {{>if i.visible}}
          {{ i.text }}
        {{<if }}
      {{<for }}
    </div>
    """.dedent

    tmpl = initTemplate(tmplString, initConfig())
    context = %* [
      {"visible": true, "text": "text one"},
      {"visible": false, "text": "text two"},
      {"visible": true, "text": "text three"},
      {"visible": false, "text": "text four"}
    ]

  doAssert tmpl.render(context) == """
    <div>
          text one
          text three
    </div>
    """.dedent
