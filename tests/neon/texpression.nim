import std/[json, strutils]
import ../../src/neon/expression
import ../../src/neon/exceptions
import ../../src/neon/stack


## only left operand of context variable and boolean
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkContextVariable, name: "$")
    )
    context = %* true
  
  var stack = Stack()
  
  doAssert expression.eval(context, stack)


## only left operand of context variable and not a boolean
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkContextVariable, name: "$")
    )
    context = %* 123

  var stack = Stack()

  doAssertRaises(ExpressionError):
    discard expression.eval(context, stack)


## only left operand of scoped variable and boolean
block:
  let 
    expression = Expression(
      leftOperand: Operand(kind:odkScopedVariable, name: "variable")
    )
    
  var
    context: JsonNode
    stack = Stack()

  stack.push("variable",  %* false)

  doAssert not expression.eval(context, stack)


## only left operand of scoped variable and not a boolean
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkScopedVariable, name: "variable")
    )

  var
    context: JsonNode
    stack = Stack()

  stack.push("variable",  %* 123)

  doAssertRaises(ExpressionError):
    discard expression.eval(context, stack)


## only left operand of value and boolean
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* true)
    )
    
  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## only left operand of value and not a boolean
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123)
    )
    
  var
    context: JsonNode
    stack = Stack()

  doAssertRaises(ExpressionError):
    discard expression.eval(context, stack)


## operands for different type
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* true),
      operator: otkEqual,
      rightOperand: Operand(kind:odkValue, value: %* 123)
    )
    
  var
    context: JsonNode
    stack = Stack()

  doAssertRaises(ExpressionError):
    discard expression.eval(context, stack)


## equal operator and equal booleans
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* nil),
      operator: otkEqual,
      rightOperand: Operand(kind:odkValue, value: %* nil)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## invalid operators for null
block:
  var
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* nil),
      rightOperand: Operand(kind:odkValue, value: %* nil)
    )

  var
    context: JsonNode
    stack = Stack()

  for operator in OperatorKind:
    if $operator notin ["", "=="]:
      expression.operator = operator

      doAssertRaises(ExpressionError):
        discard expression.eval(context, stack)


## equal operator and equal booleans
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* true),
      operator: otkEqual,
      rightOperand: Operand(kind:odkValue, value: %* true)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## equal operator and equal booleans
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* true),
      operator: otkEqual,
      rightOperand: Operand(kind:odkValue, value: %* false)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## not equal operator and equal booleans
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* true),
      operator: otkNotEqual,
      rightOperand: Operand(kind:odkValue, value: %* true)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## not equal operator and not equal booleans
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* true),
      operator: otkNotEqual,
      rightOperand: Operand(kind:odkValue, value: %* false)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## greater than equal operator, left true, right true
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* true),
      operator: otkGreaterThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* true)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## greater than equal operator, left true, right false
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* true),
      operator: otkGreaterThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* false)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## greater than equal operator, left false, right true
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* false),
      operator: otkGreaterThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* true)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## less than equal operator, left true, right true
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* true),
      operator: otkLessThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* true)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## less than equal operator, left true, right false
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* true),
      operator: otkLessThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* false)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## less than equal operator, left false, right true
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* false),
      operator: otkLessThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* true)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## greater than operator, left true, right true
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* true),
      operator: otkGreaterThan,
      rightOperand: Operand(kind:odkValue, value: %* true)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## greater than operator, left true, right false
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* true),
      operator: otkGreaterThan,
      rightOperand: Operand(kind:odkValue, value: %* false)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## greater than operator, left false, right true
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* false),
      operator: otkGreaterThan,
      rightOperand: Operand(kind:odkValue, value: %* true)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## less than operator, left true, right true
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* true),
      operator: otkLessThan,
      rightOperand: Operand(kind:odkValue, value: %* true)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## less than operator, left true, right false
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* true),
      operator: otkLessThan,
      rightOperand: Operand(kind:odkValue, value: %* false)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## less than operator, left false, right true
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* false),
      operator: otkLessThan,
      rightOperand: Operand(kind:odkValue, value: %* true)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## equal operator, int left equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123),
      operator: otkEqual,
      rightOperand: Operand(kind:odkValue, value: %* 123)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## equal operator, int, left not equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123),
      operator: otkEqual,
      rightOperand: Operand(kind:odkValue, value: %* 456)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## not equal operator, int, left equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123),
      operator: otkNotEqual,
      rightOperand: Operand(kind:odkValue, value: %* 123)
    )

  var
    context: JsonNode
    stack: Stack

  doAssert not expression.eval(context, stack)


## not equal operator, int, left not equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123),
      operator: otkNotEqual,
      rightOperand: Operand(kind:odkValue, value: %* 456)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## greater than equal operator, int, left equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123),
      operator: otkGreaterThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* 123)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## greater than equal operator, int, left greater right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 456),
      operator: otkGreaterThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* 123)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## greater than equal operator, int, right grater left
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123),
      operator: otkGreaterThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* 456)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## less than equal operator, int, left equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123),
      operator: otkLessThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* 123)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## less than equal operator, int, left greater right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 456),
      operator: otkLessThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* 123)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## less than equal operator, int, left less right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123),
      operator: otkLessThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* 456)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## greater than operator, int, left equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123),
      operator: otkGreaterThan,
      rightOperand: Operand(kind:odkValue, value: %* 123)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## greater than operator, int, left greater right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 456),
      operator: otkGreaterThan,
      rightOperand: Operand(kind:odkValue, value: %* 123)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## greater than operator, int, left less right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123),
      operator: otkGreaterThan,
      rightOperand: Operand(kind:odkValue, value: %* 456)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## less than operator, int, left equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123),
      operator: otkLessThan,
      rightOperand: Operand(kind:odkValue, value: %* 123)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## less than operator, int, left greater right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 456),
      operator: otkLessThan,
      rightOperand: Operand(kind:odkValue, value: %* 123)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## less than operator, int, left less right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123),
      operator: otkLessThan,
      rightOperand: Operand(kind:odkValue, value: %* 456)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## equal operator, float left equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123.1),
      operator: otkEqual,
      rightOperand: Operand(kind:odkValue, value: %* 123.1)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## equal operator, float, left not equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123.1),
      operator: otkEqual,
      rightOperand: Operand(kind:odkValue, value: %* 456.1)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## not equal operator, float, left equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123.1),
      operator: otkNotEqual,
      rightOperand: Operand(kind:odkValue, value: %* 123.1)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## not equal operator, float, left not equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123.1),
      operator: otkNotEqual,
      rightOperand: Operand(kind:odkValue, value: %* 456.1)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## greater than equal operator, float, left equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123.1),
      operator: otkGreaterThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* 123.1)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## greater than equal operator, float, left greater right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 456.1),
      operator: otkGreaterThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* 123.1)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## greater than equal operator, float, right grater left
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123.1),
      operator: otkGreaterThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* 456.1)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## less than equal operator, float, left equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123.1),
      operator: otkLessThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* 123.1)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## less than equal operator, float, left greater right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 456.1),
      operator: otkLessThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* 123.1)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## less than equal operator, float, left less right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123.1),
      operator: otkLessThanEqual,
      rightOperand: Operand(kind:odkValue, value: %* 456.1)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## greater than operator, float, left equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123.1),
      operator: otkGreaterThan,
      rightOperand: Operand(kind:odkValue, value: %* 123.1)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## greater than operator, float, left greater right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 456.1),
      operator: otkGreaterThan,
      rightOperand: Operand(kind:odkValue, value: %* 123.1)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## greater than operator, float, left less right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123.1),
      operator: otkGreaterThan,
      rightOperand: Operand(kind:odkValue, value: %* 456.1)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## less than operator, float, left equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123.1),
      operator: otkLessThan,
      rightOperand: Operand(kind:odkValue, value: %* 123.1)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## less than operator, float, left greater right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 456.1),
      operator: otkLessThan,
      rightOperand: Operand(kind:odkValue, value: %* 123.1)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## less than operator, float, left less right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* 123.1),
      operator: otkLessThan,
      rightOperand: Operand(kind:odkValue, value: %* 456.1)
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## equal operator, string left equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* "a"),
      operator: otkEqual,
      rightOperand: Operand(kind:odkValue, value: %* "a")
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## equal operator, float, left not equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* "a"),
      operator: otkEqual,
      rightOperand: Operand(kind:odkValue, value: %* "b")
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## not equal operator, string, left equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* "a"),
      operator: otkNotEqual,
      rightOperand: Operand(kind:odkValue, value: %* "a")
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## not equal operator, string, left not equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* "a"),
      operator: otkNotEqual,
      rightOperand: Operand(kind:odkValue, value: %* "b")
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## invalid operators for strings
block:
  var
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* "a"),
      rightOperand: Operand(kind:odkValue, value: %* "b")
    )

  var
    context: JsonNode
    stack = Stack()

  for operator in OperatorKind:
    if $operator notin ["", "==", "!="]:
      expression.operator = operator

      doAssertRaises(ExpressionError):
        discard expression.eval(context, stack)

## equal operator, array left equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* [1, 2]),
      operator: otkEqual,
      rightOperand: Operand(kind:odkValue, value: %* [1, 2])
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## equal operator, array, left not equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* [1, 2]),
      operator: otkEqual,
      rightOperand: Operand(kind:odkValue, value: %* [1, 3])
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## not equal operator, array, left equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* [1, 1]),
      operator: otkNotEqual,
      rightOperand: Operand(kind:odkValue, value: %* [1, 1])
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## not equal operator, array, left not equal right
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* [1, 1]),
      operator: otkNotEqual,
      rightOperand: Operand(kind:odkValue, value: %* [1, 2])
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## invalid operators for arrays
block:
  var
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* [1, 2]),
      rightOperand: Operand(kind:odkValue, value: %* [1, 3])
    )

  var
    context: JsonNode
    stack = Stack()

  for operator in OperatorKind:
    if $operator notin ["", "==", "!="]:
      expression.operator = operator

      doAssertRaises(ExpressionError):
        discard expression.eval(context, stack)


## equal operator, object, keys and values equal
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* {"key": 123}),
      operator: otkEqual,
      rightOperand: Operand(kind:odkValue, value: %* {"key": 123})
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## equal operator, object, keys and values not equal
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* {"key": 123}),
      operator: otkEqual,
      rightOperand: Operand(kind:odkValue, value: %* {"key": 456})
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## equal operator, object, keys not equal and values equal
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* {"keyOne": 123}),
      operator: otkEqual,
      rightOperand: Operand(kind:odkValue, value: %* {"keyTwo": 123})
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## not equal operator, object, keys and values equal
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* {"key": 123}),
      operator: otkNotEqual,
      rightOperand: Operand(kind:odkValue, value: %* {"key": 123})
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert not expression.eval(context, stack)


## not equal operator, object, keys and values not equal
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* {"key": 123}),
      operator: otkNotEqual,
      rightOperand: Operand(kind:odkValue, value: %* {"key": 456})
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## not equal operator, object, keys not equal and values equal
block:
  let
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* {"keyOne": 123}),
      operator: otkNotEqual,
      rightOperand: Operand(kind:odkValue, value: %* {"keyTwo": 123})
    )

  var
    context: JsonNode
    stack = Stack()

  doAssert expression.eval(context, stack)


## invalid operators for objects
block:
  var
    expression = Expression(
      leftOperand: Operand(kind:odkValue, value: %* {"keyOne": 123}),
      rightOperand: Operand(kind:odkValue, value: %* {"keyTwo": 123})
    )

  var
    context: JsonNode
    stack = Stack()

  for operator in OperatorKind:
    if $operator notin ["", "==", "!="]:
      expression.operator = operator

      doAssertRaises(ExpressionError):
        discard expression.eval(context, stack)
