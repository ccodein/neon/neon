import std/[strutils]
import ../../src/neon/token
import ../../src/neon/pattern
import ../../src/neon/node
import ../../src/neon/parse


## context variable
block:
  let
    tmpl = """
    {{ $ }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert $rootNode.nodes[0] ==
    """
    kind: nkContextVariable
    name: $
    start token:
      kind: tkContextVariable
      match: "{{ $ }}"
      position: 0 .. 6""".dedent

## scoped variable
block:
  let
    tmpl = """
    {{ variable }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)


  doAssert $rootNode.nodes[0] ==
    """
    kind: nkScopedVariable
    name: variable
    start token:
      kind: tkScopedVariable
      match: "{{ variable }}"
      position: 0 .. 13""".dedent


## if with context
block:
  let
    tmpl = """
    {{>if $ }}
      body
    {{<if  }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert $rootNode.nodes[0] ==
    """
    kind: nkIf
    node count: 0
    expression: $
    start token:
      kind: tkIfStart
      match: "{{>if $ }}\x0A"
      position: 0 .. 10
    stop token:
      kind: tkIfStop
      match: "{{<if  }}\x0A"
      position: 18 .. 27""".dedent


## if with context
block:
  let
    tmpl = """
    {{>if true }}
      body
    {{<if  }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert $rootNode.nodes[0] ==
    """
    kind: nkIf
    node count: 0
    expression: true
    start token:
      kind: tkIfStart
      match: "{{>if true }}\x0A"
      position: 0 .. 13
    stop token:
      kind: tkIfStop
      match: "{{<if  }}\x0A"
      position: 21 .. 30""".dedent


## if with variable
block:
  let
    tmpl = """
    {{>if variable }}
      body
    {{<if  }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert $rootNode.nodes[0] ==
    """
    kind: nkIf
    node count: 0
    expression: variable
    start token:
      kind: tkIfStart
      match: "{{>if variable }}\x0A"
      position: 0 .. 17
    stop token:
      kind: tkIfStop
      match: "{{<if  }}\x0A"
      position: 25 .. 34""".dedent


## if with variable, operator and value
block:
  let
    tmpl = """
    {{>if variable==123 }}
      body
    {{<if  }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert $rootNode.nodes[0] ==
    """
    kind: nkIf
    node count: 0
    expression: variable==123
    start token:
      kind: tkIfStart
      match: "{{>if variable==123 }}\x0A"
      position: 0 .. 22
    stop token:
      kind: tkIfStop
      match: "{{<if  }}\x0A"
      position: 30 .. 39""".dedent


## if with context, operator and value
block:
  let
    tmpl = """
    {{>if $==123 }}
      body
    {{<if  }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert $rootNode.nodes[0] ==
    """
    kind: nkIf
    node count: 0
    expression: $==123
    start token:
      kind: tkIfStart
      match: "{{>if $==123 }}\x0A"
      position: 0 .. 15
    stop token:
      kind: tkIfStop
      match: "{{<if  }}\x0A"
      position: 23 .. 32""".dedent


## if with value, operator and variable
block:
  let
    tmpl = """
    {{>if 123==variable }}
      body
    {{<if  }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert $rootNode.nodes[0] ==
    """
    kind: nkIf
    node count: 0
    expression: 123==variable
    start token:
      kind: tkIfStart
      match: "{{>if 123==variable }}\x0A"
      position: 0 .. 22
    stop token:
      kind: tkIfStop
      match: "{{<if  }}\x0A"
      position: 30 .. 39""".dedent


## if with value, operator and context
block:
  let
    tmpl = """
    {{>if 123==$ }}
      body
    {{<if  }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert $rootNode.nodes[0] ==
    """
    kind: nkIf
    node count: 0
    expression: 123==$
    start token:
      kind: tkIfStart
      match: "{{>if 123==$ }}\x0A"
      position: 0 .. 15
    stop token:
      kind: tkIfStop
      match: "{{<if  }}\x0A"
      position: 23 .. 32""".dedent


## if with variable, operator and context
block:
  let
    tmpl = """
    {{>if variable==$ }}
      body
    {{<if  }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert $rootNode.nodes[0] ==
    """
    kind: nkIf
    node count: 0
    expression: variable==$
    start token:
      kind: tkIfStart
      match: "{{>if variable==$ }}\x0A"
      position: 0 .. 20
    stop token:
      kind: tkIfStop
      match: "{{<if  }}\x0A"
      position: 28 .. 37""".dedent


## elif with context
block:
  let
    tmpl = """
    {{>if $ }}
      if body
    {{>elif $ }}
      elif body
    {{<if  }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert $rootNode.nodes[0].nodes[0] ==
    """
    kind: nkElif
    node count: 0
    expression: $
    start token:
      kind: tkElifStart
      match: "{{>elif $ }}\x0A"
      position: 21 .. 33
    stop token:
      kind: tkIfStop
      match: "{{<if  }}\x0A"
      position: 46 .. 55""".dedent


## else
block:
  let
    tmpl = """
    {{>if $ }}
      if body
    {{>else }}
      else body
    {{<if  }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert $rootNode.nodes[0].nodes[0] ==
    """
    kind: nkElse
    node count: 0
    start token:
      kind: tkElseStart
      match: "{{>else }}\x0A"
      position: 21 .. 31
    stop token:
      kind: tkIfStop
      match: "{{<if  }}\x0A"
      position: 44 .. 53""".dedent


## for
block:
  let
    tmpl = """
    {{>for i in $ }}
      body
    {{<for  }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert $rootNode.nodes[0] ==
    """
    kind: nkFor
    node count: 0
    expression: i in $
    start token:
      kind: tkForStart
      match: "{{>for i in $ }}\x0A"
      position: 0 .. 16
    stop token:
      kind: tkForStop
      match: "{{<for  }}\x0A"
      position: 24 .. 34""".dedent


## comment
block:
  let
    tmpl = """
    {{># }}
      body
    {{<#  }}
    """.dedent

    regex = initTokensRegex(r"\{\{", r"\}\}")
    tokens = tmpl.findTokens(regex)
    rootNode = tokens.parseTokens(tmpl)

  doAssert $rootNode.nodes[0] ==
    """
    kind: nkComment
    start token:
      kind: tkCommentStart
      match: "{{># }}\x0A"
      position: 0 .. 7
    stop token:
      kind: tkCommentStop
      match: "{{<#  }}\x0A"
      position: 15 .. 23""".dedent


## root
block:
  let
    rootNode = Node(kind: nkRoot)

  doAssert $rootNode ==
    """
    kind: nkRoot""".dedent
