import std/[json]
import ../../src/neon/context
import ../../src/neon/exceptions


## get value of the context variable
block:
  let context = %* true
  
  doAssert context.value("$") == %* true


## get value of the context variable key
block:
  let context = %* {"key": true}
  
  doAssert context.value("$.key") == %* true


## get value of the context variable sub-key
block:
  let context = %* {"key": {"nestedKey": true}}

  doAssert context.value("$.key.nestedKey") == %* true

## get value of the context variable
block:
  let context = %* true
  
  doAssert context.value("$") == %* true


## the context variable key does not exist
block:
  let context = %* {}

  doAssertRaises(ContextError):
    discard context.value("$.key")


## the context variable key is not an object
block:
  let context = %* {"key": 123}

  doAssertRaises(ContextError):
    discard context.value("$.key.key")

