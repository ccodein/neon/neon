version       = "0.1.0"
author        = "lluar"
description   = "A runtime template engine."
license       = "MIT"
srcDir        = "src"


requires "nim >= 1.6.12"


import os

task genDoc, "Generate documentation":
  rmDir("htmldocs")
  
  for file in listFiles("doc"):
    if splitFile(file).ext == ".md":
      exec "nim rst2html --index:on --outdir:htmldocs " & file

  exec "nim doc --project --outdir:htmldocs src/neon.nim"
  exec "nim buildIndex -o:htmldocs/theindex.html htmldocs"
