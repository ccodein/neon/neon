type
  TemplateFormatError* = object of CatchableError
  TokenNotOpenedError* = object of TemplateFormatError
  TokenNotClosedError* = object of TemplateFormatError
  TokenHasNoBodyError* = object of TemplateFormatError
  ParentTokenError* = object of TemplateFormatError
  TypeError* = object of TemplateFormatError
  ExpressionError* = object of TemplateFormatError
  StackError* = object of TemplateFormatError

  KeyNotExistsError* = object of CatchableError
  NotAnObjectError* = object of CatchableError
  ContextError* = object of CatchableError


