import std/[re, json, strutils]
import token
import pattern
import exceptions
import expression
import node

export expression
export node


proc parseExpression(data: string): Expression
    {.raises: [ExpressionError].} =
  var
    parts: seq[string]
    matches: array[1, string]

  for kind in OperatorKind:
    if kind != otkNone:
      parts = data.split($kind)

      if parts.len == 2:
        result.operator = kind
        break

  if parts[0].match(contextVariableRegex, matches):
    result.leftOperand = Operand(kind: odkContextVariable, name: parts[0])

  elif parts[0].match(scopedVariableRegex, matches):
    result.leftOperand = Operand(kind: odkScopedVariable, name: parts[0])

  else:
    try:
      result.leftOperand = Operand(kind: odkValue, value: parseJson(parts[0]))

    except Exception:
      raise newException(ExpressionError,
        "The left operand \"" & parts[0] & "\" of the expression \"" & data &
        "\" must be in correct JSON, a scoped varable or a context variable.")

  if result.operator != otkNone:
    if parts[1].match(contextVariableRegex, matches):
      result.rightOperand = Operand(kind: odkContextVariable, name: parts[1])

    elif parts[1].match(scopedVariableRegex, matches):
      result.rightOperand = Operand(kind: odkScopedVariable, name: parts[1])

    else:
      try:
        result.rightOperand = Operand(kind: odkValue, value: parseJson(parts[1]))
      except Exception:
        raise newException(ExpressionError,
          "The right operand \"" & parts[0] & "\" of the expression \"" & data &
          "\" must be in correct JSON, a scoped varable or a context variable.")


proc parseForVariables(data: string): ForVariables
    {.raises: [TypeError].} =
  let parts = data.split("for")[1].split("in")
  var matches: array[1, string]

  result.variable = parts[0].strip

  if parts[1].strip().match(contextVariableRegex, matches):
    result.iterable = Iterable(kind: ikContextVariable, name: matches[0])

  elif parts[1].strip().match(scopedVariableRegex, matches):
    result.iterable = Iterable(kind: ikScopedVariable, name: matches[0])

  else:
    result.iterable = Iterable(kind: ikValue)

    try:
      result.iterable.value = parseJson(parts[1])

    except Exception:
      raise newException(TypeError,
        "The iterable \"" & parts[1] & "\" of the expression \"" & data &
        "\" must be a JSON array.")


proc parseTokens*(tokens: Tokens, tmpl: string): Node
    {.raises: [
      ExpressionError,
      TypeError,
      TokenHasNoBodyError,
      TokenNotClosedError,
      TokenNotOpenedError,
      ParentTokenError].} =
  result = Node()

  var
    node = result
    isComment = false

  for token in tokens:
    case token.kind:
      of tkContextVariable:
        if not isComment:
          let name = token.match.strip()[2..^3].strip

          node.nodes.add(
            Node(
              kind: nkContextVariable,
              name: name,
              parent: node,
              startToken: token
            )
          )

      of tkScopedVariable:
        if not isComment:
          let name = token.match.strip()[2..^3].strip

          node.nodes.add(
            Node(
              kind: nkScopedVariable,
              name: name,
              parent: node,
              startToken: token
            )
          )

      of tkCommentStart:
        let commentNode = Node(
          kind: nkComment,
          parent: node,
          startToken: token
        )

        node.nodes.add(commentNode)
        isComment = true
        node = commentNode

      of tkCommentStop:
        node.stopToken = token

        if node.stopToken.position.a-1 == node.startToken.position.b:
          raise newException(TokenHasNoBodyError,
            "The tag: \n" & $node.startToken & "\nhas no body.")

        if node.kind != nkComment:
          raise newException(TokenNotOpenedError,
            "Tag mismatch in the template. The tag:\n" & $token &
            "\nwas never opened. Perhaps the opening tag is not in the " &
            "correct format and was not found.")

        node = node.parent
        isComment = false

      of tkIfStart:
        if not isComment:
          let ifNode = Node(
            kind: nkIf,
            parent: node,
            startToken: token
          )

          ifNode.expression = token.match.strip().replace(" ", "")[5..^3].parseExpression
          node.nodes.add(ifNode)
          node = ifNode

      of tkElifStart:
        if not isComment:
          let elifNode = Node(
            kind: nkElif,
            startToken: token,
            expression: token.match.strip.replace(" ", "")[7..^3].parseExpression
          )

          if node.kind == nkIf:
            elifNode.parent = node

            if elifNode.startToken.position.a-1 == node.startToken.position.b:
              raise newException(TokenHasNoBodyError,
                "The tag: \n" & $node.startToken & "\nhas no body.")

            node.nodes.add(elifNode)

          elif node.kind == nkElif:
            elifNode.parent = node.parent
            node.stopToken = token


            if node.stopToken.position.a-1 == node.startToken.position.b:
              raise newException(TokenHasNoBodyError,
                "The tag: \n" & $node.startToken & "\nhas no body.")

            node.parent.nodes.add(elifNode)

          else:
            raise newException(ParentTokenError,
              "The \"elif\" tag\n" & $token & "\nmust be inside \"if\" tags."
            )

          node = elifNode

      of tkElseStart:
        if not isComment:
          let elseNode = Node(
            kind: nkElse,
            startToken: token
          )


          if node.kind == nkIf:
            if elseNode.startToken.position.a-1 == node.startToken.position.b:
              raise newException(TokenHasNoBodyError,
                "The tag: \n" & $node.startToken & "\nhas no body.")

            elseNode.parent = node
            node.nodes.add(elseNode)

          elif node.kind == nkElif:
            node.stopToken = token
            elseNode.parent = node.parent


            if elseNode.startToken.position.a-1 == node.startToken.position.b:
              raise newException(TokenHasNoBodyError,
                "The tag: \n" & $node.startToken & "\nhas no body.")

            node.parent.nodes.add(elseNode)

          else:
            raise newException(ParentTokenError,
              "The \"else\" tag\n" & $token & "\nmust be inside \"if\" tags."
            )

          node = elseNode

      of tkIfStop:
        if not isComment:
          if node.kind in [nkElif, nkElse]:
            node.stopToken = token
            node.parent.stopToken = token

            if node.stopToken.position.a-1 == node.startToken.position.b:
              raise newException(TokenHasNoBodyError,
                "The tag: \n" & $node.startToken & "\nhas no body.")

            node = node.parent.parent

          elif node.kind == nkIf:
            node.stopToken = token

            if node.stopToken.position.a-1 == node.startToken.position.b:
              raise newException(TokenHasNoBodyError,
                "The tag: \n" & $node.startToken & "\nhas no body.")

            node = node.parent

          else:
            raise newException(TokenNotOpenedError,
              "Tag mismatch in the template. The tag:\n" & $token &
              "\nwas never opened. Perhaps the opening tag is not in the " &
              "correct format and was not found."
            )

      of tkForStart:
        if not isComment:
          let forNode = Node(
            kind: nkFor,
            parent: node,
            startToken: token,
            variables: token.match.strip()[2..^3].strip[1..^1].strip.parseForVariables
          )

          node.nodes.add(forNode)
          node = forNode

      of tkForStop:
        if not isComment:
          if node.kind != nkFor:
            raise newException(TokenNotOpenedError,
              "Tag mismatch in the template. The tag:\n" & $token &
              "\nwas never opened. Perhaps the opening tag is not in the " &
              "correct format and was not found."
            )
          else:
            node.stopToken = token

            if node.stopToken.position.a-1 == node.startToken.position.b:
              raise newException(TokenHasNoBodyError,
                "The tag: \n" & $node.startToken & "\nhas no body.")

          node = node.parent

  if node.kind != nkRoot:
    raise newException(TokenNotClosedError,
      "The tag:\n" & $node.startToken & "\nwas not closed."
    )
