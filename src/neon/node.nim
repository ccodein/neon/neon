import std/[json, strutils]
import token
import expression

export expression


type
  IterableKind* = enum
    ikContextVariable
    ikScopedVariable
    ikValue

  Iterable* = object
    case kind*: IterableKind:
      of ikContextVariable, ikScopedVariable:
        name*: string
      of ikValue:
        value*: JsonNode

  ForVariables* = object
    variable*: string
    iterable*: Iterable

  NodeKind* = enum
    nkRoot
    nkContextVariable
    nkScopedVariable
    nkComment
    nkIf
    nkElif
    nkElse
    nkFor

  NodeBase = object of RootObj
    parent*: Node
    startToken*: Token
    stopToken*: Token
    nodes*: seq[Node]

  Node* = ref object of NodeBase
    case kind*: NodeKind
      of nkContextVariable, nkScopedVariable:
        name*: string
      of nkIf, nkElif:
        expression*: Expression
      of nkFor:
        variables*: ForVariables
      else: discard


func `$`*(node: Node): string {.raises: [].} =
  result.add "kind: " & $node.kind

  if node.kind in [nkif, nkElif, nkElse, nkFor]:
      result.add "\nnode count: " & $node.nodes.len

  case node.kind:
    of nkContextVariable, nkScopedVariable:
      result.add "\nname: " & $node.name

    of nkIf, nkElif:
      let operator = node.expression.operator
      let leftOperand = node.expression.leftOperand
      let rightOperand = node.expression.rightOperand

      if operator == otkNone:
        if leftOperand.kind == odkValue:
          result.add "\nexpression: " & $leftOperand.value
        else:
          result.add "\nexpression: " & leftOperand.name

      else:
        if leftOperand.kind != odkValue and rightOperand.kind == odkValue:
          result.add "\nexpression: " & leftOperand.name & $operator &
          $rightOperand.value
        elif leftOperand.kind == odkValue and rightOperand.kind != odkValue:
          result.add "\nexpression: " & $leftOperand.value & $operator &
          rightOperand.name
        else:
          result.add "\nexpression: " & leftOperand.name & $operator &
          rightOperand.name

    of nkFor:
      result.add "\nexpression: " & $node.variables.variable & " in " &
        $node.variables.iterable.name

    else: discard

  if node.startToken != nil:
    result.add "\nstart token:\n" & indent($node.startToken, 2)

  if node.stopToken != nil:
    result.add "\nstop token:\n" & indent($node.stopToken, 2)

