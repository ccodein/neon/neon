import std/[json, strutils]
import exceptions
import utils


type
  StackItem = object
    name: string
    value: JsonNode

  Stack* = ref object
    items: seq[StackItem]


func push*(stack: Stack, variableName: string, value: JsonNode)
    {.raises: [].} =
  stack.items.add(StackItem(name: variableName, value: value))


func pop*(stack: Stack, variableName: string)
    {.raises: [StackError].} =
  for i in countdown(stack.items.high, 0):
    if stack.items[i].name == variableName:
      stack.items.del(i)
      return

  raise newException(StackError,
    "Cannot remove the Variable. The scoped variable \"" & variableName &
    "\" does not exist on the template stack.")

func update*(stack: Stack, variableName: string, value: JsonNode)
    {.raises: [StackError].} =
  for i in countdown(stack.items.high, 0):
    if stack.items[i].name == variableName:
      stack.items[i].value = value
      return

  raise newException(StackError,
    "Cannot update the variable. The scoped variable \"" & variableName &
    "\" does not exist on the template stack.")

proc value*(stack: Stack, variableName: string): JsonNode
    {.raises: [StackError].} =
  let keys = variableName.split(".", maxsplit=1)

  for i in countdown(stack.items.high, 0):
    if stack.items[i].name == keys[0]:
      result = stack.items[i].value

      if keys.len > 1:
        try:
          result = result.valueByDotNotation(keys[1])

        except NotAnObjectError, KeyNotExistsError:
          let ex = getCurrentException()

          raise newException(StackError,
            "The scoped variable does not exist on the template stack. " &
            ex.msg)

      return

  raise newException(StackError,
    "The scoped variable does not exist on the template stack.")



