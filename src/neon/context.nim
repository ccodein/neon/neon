import std/[json, strutils]
import utils
import exceptions


func value*(context: JsonNode, variableName: string): JsonNode
    {.raises: [ContextError].} =
  let keys = variableName.split(".", maxsplit=1)

  if keys.len == 1:
    return context
  else:
    try:
      return context.valueByDotNotation(keys[1])

    except KeyNotExistsError as ex:
      raise newException(ContextError,
        "The context variable \"" & keys[1] & "\" does not exist. " & ex.msg)

    except NotAnObjectError as ex:
      raise newException(ContextError,
        "The context variable \"" & keys[1] & "\" does not exist. " & ex.msg)
