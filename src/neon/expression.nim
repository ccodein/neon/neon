import std/[json]
import exceptions
import stack
import context


type
  OperatorKind* = enum
    otkNone = ""
    otkEqual = "=="
    otkNotEqual = "!="
    otkGreaterThanEqual = ">="
    otkLessThanEqual = "<="
    otkGreaterThan = ">"
    otkLessThan = "<"

  OperandKind* = enum
    odkContextVariable
    odkScopedVariable
    odkValue

  Operand* = object
    case kind*: OperandKind:
      of odkContextVariable, odkScopedVariable:
        name*: string
      of odkValue:
        value*: JsonNode

  Expression* = object
    leftOperand*: Operand
    operator*: OperatorKind
    rightOperand*: Operand



proc eval*(
  expression: Expression, context: JsonNode, stack: var Stack): bool
    {.raises: [StackError, ContextError, ExpressionError].} =
  var leftValue, rightValue: JsonNode

  case expression.leftOperand.kind:
    of odkContextVariable:
      leftValue = context.value(expression.leftOperand.name)

    of odkScopedVariable:
      leftValue = stack.value(expression.leftOperand.name)

    of odkValue:
      leftValue = expression.leftOperand.value

  if expression.operator == otkNone:
    if leftValue.kind == JBool:
      return leftValue.getBool

    else:
      raise newException(ExpressionError,
        "The the expression must be of type bool.")

  else:
    case expression.rightOperand.kind:
      of odkContextVariable:
        rightValue = context.value(expression.rightOperand.name)

      of odkScopedVariable:
        rightValue = stack.value(expression.rightOperand.name)

      of odkValue:
        rightValue = expression.rightOperand.value

    if leftValue.kind != rightValue.kind:
      raise newException(ExpressionError,
        "Both operands of the expression must be of the same type.")

    case leftValue.kind:
      of JNull:
        case expression.operator:
          of otkEqual:
            return true
          
          else:
            raise newException(ExpressionError,
              "The operator \"" & $expression.operator & "\" is not " &
              "defined for value of type \"null\".")

      of JBool:
        case expression.operator:
          of otkEqual:
            return leftValue.getBool == rightValue.getBool

          of otkNotEqual:
            return leftValue.getBool != rightValue.getBool

          of otkGreaterThanEqual:
            return leftValue.getBool >= rightValue.getBool

          of otkLessThanEqual:
            return leftValue.getBool <= rightValue.getBool

          of otkGreaterThan:
            return leftValue.getBool > rightValue.getBool

          of otkLessThan:
            return leftValue.getBool < rightValue.getBool

          else: discard
      of JInt:
        case expression.operator:
          of otkEqual:
            return leftValue.getInt == rightValue.getInt

          of otkNotEqual:
            return leftValue.getInt != rightValue.getInt

          of otkGreaterThanEqual:
            return leftValue.getInt >= rightValue.getInt

          of otkLessThanEqual:
            return leftValue.getInt <= rightValue.getInt

          of otkGreaterThan:
            return leftValue.getInt > rightValue.getInt

          of otkLessThan:
            return leftValue.getInt < rightValue.getInt

          else: discard
      of JFloat:
        case expression.operator:
          of otkEqual:
            return leftValue.getFloat == rightValue.getFloat

          of otkNotEqual:
            return leftValue.getFloat != rightValue.getFloat

          of otkGreaterThanEqual:
            return leftValue.getFloat >= rightValue.getFloat

          of otkLessThanEqual:
            return leftValue.getFloat <= rightValue.getFloat

          of otkGreaterThan:
            return leftValue.getFloat > rightValue.getFloat

          of otkLessThan:
            return leftValue.getFloat < rightValue.getFloat

          else: discard

      of JString:
        case expression.operator:
          of otkEqual:
            return leftValue.getStr == rightValue.getStr

          of otkNotEqual:
            return leftValue.getStr != rightValue.getStr

          else:
            raise newException(ExpressionError,
              "The operator \"" & $expression.operator & "\" is not " &
              "defined for value of type \"string\".")

      of JArray:
        case expression.operator:
          of otkEqual:
            try:
              return leftValue.elems == rightValue.elems
            except Exception: discard

          of otkNotEqual:
            try:
              return leftValue.elems != rightValue.elems
            except Exception: discard

          else:
            raise newException(ExpressionError,
              "The operator \"" & $expression.operator & "\" is not " &
              "defined for value of type \"array\".")

      of JObject:
        case expression.operator:
          of otkEqual:
            try:
              return leftValue.fields == rightValue.fields
            except Exception: discard

          of otkNotEqual:
            try:
              return leftValue.fields != rightValue.fields
            except Exception: discard

          else:
            raise newException(ExpressionError,
              "The operator \"" & $expression.operator & "\" is not " &
              "defined for value of type \"object\".")

