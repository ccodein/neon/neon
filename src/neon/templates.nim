import std/[strutils, enumerate]
import token
import pattern
import parse
import exceptions


type
  Config = object
    startSequence: string
    stopSequence: string


proc initConfig*(startSequence = r"\{\{", stopSequence = r"\}\}"): Config
    {.raises: [].} =
  result.startSequence = startSequence
  result.stopSequence = stopSequence


type
  Template* = object
    data*: string
    tokens*: Tokens
    node*: Node
    tokensRegex*: TokensRegex


proc initTemplate*(data: string, config = initConfig()): Template
    {.raises: [TemplateFormatError].} =
  result.data = data
  result.tokensRegex = initTokensRegex(
    config.startSequence, config.stopSequence)
  result.tokens = findTokens(data, result.tokensRegex)
  result.node = result.tokens.parseTokens(data)


proc `$`*(tmpl: Template): string {.raises: [].} =
  var
    indentCount = 0
    treeRepr: string

  proc innerEchoNode(node: Node) =
    treeRepr.add ($node).indent(indentCount)

    if node.nodes.len > 0:
      treeRepr.add "\nnodes:\n------".indent(indentCount)

    indentCount += 2

    for i, n in enumerate(node.nodes):
      treeRepr.add ("\n\nindex: " & $i).indent(indentCount) & "\n"
      n.innerEchoNode

    indentCount -= 2

  tmpl.node.innerEchoNode
  result = treeRepr
