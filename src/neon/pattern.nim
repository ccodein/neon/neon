import std/[re, strutils]


let
  definitions = """(?(DEFINE)
  (?<JSON>(?>\s*(?&VALUE)\s*|\s*(?&OBJECT)\s*|\s*(?&ARRAY)\s*))
  (?<OBJECT>(?>\{\s*(?>(?&PAIR)(?>\s*,\s*(?&PAIR))*)?\s*\}))
  (?<PAIR>(?>(?&STRING)\s*:\s*(?&VALUE)))
  (?<ARRAY>(?>\[\s*(?>(?&VALUE)(?>\s*,\s*(?&VALUE))*)?\s*\]))
  (?<VALUE>(?>(?&BOOL)|(?&NULL)|(?&STRING)|(?&FLOAT)|(?&INT)|(?&OBJECT)|(?&ARRAY)))
  (?<STRING>(?>"(?>\\(?>["\\\/bfnrt]|u[a-fA-F0-9]{4})|[^"\\\0-\x1F\x7F]+)*"))
  (?<INT>(?>[+-]?[1-9]\d*|0))
  (?<FLOAT>(?>(?>[+-]?[0-9]+\.[0-9]+)(?>[eE]?[+-]?[0-9]+)?))
  (?<BOOL>(?>true|false))
  (?<NULL>(?>null))
  (?<SCOPED_VARIABLE>\b(?!(?:true|false))\b[a-zA-Z]\w*(?:.[a-zA-Z]\w*)*)
  (?<CONTEXT_VARIABLE>\$(?:\.[a-zA-Z]\w*)*)
  (?<OPERATOR>(==)|(!=)|(>=)|(<=)|(>)|(<))
  (?<UNTIL_NEWLINE>(?:[ \t]*\R)?)
  (?<LEADING_SPACES>[ \t]*)
  (?<OPERAND>(?&SCOPED_VARIABLE)|(?&CONTEXT_VARIABLE)|(?&JSON))
  (?<IF_EXPRESSION>(?&OPERAND)(?:\s*(?&OPERATOR)\s*(?&OPERAND))*)
  (?<FOR_EXPRESSION>for\s+[a-zA-Z]\w*\s+in\s+(?&OPERAND))
  (?<SCOPED_VARIABLE_TOKEN>_BEGIN_\s*(?&SCOPED_VARIABLE)\s*_END_)
  (?<CONTEXT_VARIABLE_TOKEN>_BEGIN_\s*(?&CONTEXT_VARIABLE)\s*_END_)
  (?<COMMENT_START_TOKEN>(?&LEADING_SPACES)_BEGIN_\s*>\s*#\s*_END_(?&UNTIL_NEWLINE))
  (?<COMMENT_STOP_TOKEN>(?&LEADING_SPACES)_BEGIN_\s*<\s*#\s*_END_(?&UNTIL_NEWLINE))
  (?<IF_START_TOKEN>(?&LEADING_SPACES)_BEGIN_\s*>\s*if\s+(?&IF_EXPRESSION)\s*_END_(?&UNTIL_NEWLINE))
  (?<IF_STOP_TOKEN>(?&LEADING_SPACES)_BEGIN_\s*<\s*if\s*_END_(?&UNTIL_NEWLINE))
  (?<ELIF_START_TOKEN>(?&LEADING_SPACES)_BEGIN_\s*>\s*elif\s+(?&IF_EXPRESSION)\s*_END_(?&UNTIL_NEWLINE))
  (?<ELSE_START_TOKEN>(?&LEADING_SPACES)_BEGIN_\s*>\s*else\s*_END_(?&UNTIL_NEWLINE))
  (?<FOR_START_TOKEN>(?&LEADING_SPACES)_BEGIN_\s*>\s*(?&FOR_EXPRESSION)\s*_END_(?&UNTIL_NEWLINE))
  (?<FOR_STOP_TOKEN>(?&LEADING_SPACES)_BEGIN_\s*<\s*for\s*_END_(?&UNTIL_NEWLINE))
  (?<TOKENS>(?x)(?&SCOPED_VARIABLE_TOKEN)|(?&CONTEXT_VARIABLE_TOKEN)|
    (?&COMMENT_START_TOKEN)|(?&COMMENT_STOP_TOKEN)|(?&IF_START_TOKEN)|
    (?&IF_STOP_TOKEN)|(?&ELIF_START_TOKEN)|(?&ELSE_START_TOKEN)|
    (?&FOR_START_TOKEN)|(?&FOR_STOP_TOKEN))
  )"""

  scopedVariableRegex* = re("^(" & definitions & "(?&SCOPED_VARIABLE)" & ")$")
  contextVariableRegex* = re("^(" & definitions & "(?&CONTEXT_VARIABLE)" & ")$")


type
  TokensRegex* = object
    startSequence*: string
    stopSequence*: string
    regex*: Regex

proc initTokensRegex*(startSequence, stopSequence: string): TokensRegex =
  result.startSequence = startSequence
  result.stopSequence = stopSequence

  let definition = definitions.replace(
    "_BEGIN_", startSequence).replace("_END_", stopSequence)
  
  try:
    result.regex = re("(" & definition & "(?&TOKENS))")
  except RegexError:
    discard
