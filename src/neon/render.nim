import std/[json, strutils, enumerate]
import templates
import parse
import exceptions
import expression
import stack
import token

export json


func value(context: JsonNode, name: string): JsonNode
    {.raises: [ContextError].} =
  let keys = name.split(".")
  var
    contextName = "$"
    node = context

  for i, key in enumerate(keys):
    if i > 0:
      if node.kind == JObject:
        contextName.add("." & key)

        try:
          node = node[key]

        except KeyError:
          raise newException(ContextError,
            "The context variable \"" & contextName & "\" does not exist.")

      else:
        raise newException(ContextError,
          "The context variable \"" & contextName & "\" must be of kind JObject.")

  result = node


func renderValue(value: JsonNode): string
    {.raises: [].} =
  case value.kind:
    of JString:
      return value.getStr()

    else:
      return  $value


func renderContextVariable(node: Node, context: JsonNode): string
    {.raises: [ContextError].} =
  try:
    result = context.value(node.name).renderValue
  except ContextError as ex:
    ex.msg.add("\n" & $node)
    raise ex


proc renderScopedVariable(node: Node, stack: Stack): string
    {.raises: [StackError].} =
  let keys = node.name.split(".")

  var 
    variableName = keys[0]
    value: JsonNode

  try:
    value = stack.value(variableName)

  except StackError as ex:
    ex.msg.add("\n" & $node)
    raise ex

  if keys.len > 1:
    for n, key in enumerate(keys[1..^1]):
      if value.kind == JObject:
        try:
          variableName.add("." & key)
          value = value[key]

        except KeyError:
          raise newException(StackError,
            "The key \"" & key &
            "\" does not exist in the scoped variable object.\n" & $node)

      else:
        raise newException(StackError,
            "The scoped variable \"" & variableName &
            "\" is not an object.\n" & $node)

    return value.renderValue

  else:
    return value.renderValue


proc render(node: Node, context: JsonNode, stack: var Stack, tmpl: string): string
    {.raises: [ContextError, StackError, TypeError, ExpressionError].}


proc renderIf(node: Node, context: JsonNode, stack: var Stack, tmpl: string): string
    {.raises: [ContextError, StackError, TypeError, ExpressionError].} =
  try:
    if node.expression.eval(context, stack):
      result.add(node.render(context, stack, tmpl))

    else:
      for i, n in enumerate(node.nodes):
        if n.kind == nkElif and n.expression.eval(context, stack):
          result.add(n.render(context, stack, tmpl))
          break
        elif n.kind == nkElse:
          result.add(n.render(context, stack, tmpl))
          break

  except ExpressionError as ex:
    ex.msg.add("\n" & $node)
    raise ex


proc renderFor(node: Node, context: JsonNode, stack: var Stack, tmpl: string): string
    {.raises: [ContextError, StackError, ExpressionError, TypeError].} =
  let
    iterable = node.variables.iterable
    variableName = node.variables.variable

  var 
    value: JsonNode

  case iterable.kind:
    of ikValue:
      value = iterable.value

    of ikContextVariable:
      value = context.value(iterable.name)

    of ikScopedVariable:
      value = stack.value(iterable.name)


  if value.kind == JArray:
    for i, value in enumerate(value.getElems):
      if i == 0:
        stack.push(variableName, value)

      else:
        stack.update(variableName, value)

      result.add(node.render(context, stack, tmpl))

    stack.pop(variableName)

  else:
    raise newException(TypeError,
      "The iterable of the for token:\n" & $node.startToken &
      "\" is not an array.\n")


proc render(node: Node, context: JsonNode, stack: var Stack, tmpl: string): string =
  if node.nodes.len == 0:
    if node.kind == nkRoot:
      result.add tmpl

    else:
      result.add tmpl[node.startToken.position.b+1..node.stopToken.position.a-1]

  else:
    for i, node in enumerate(node.nodes):
      if i == 0:
        if node.parent.kind == nkRoot:
          result.add tmpl[0..node.startToken.position.a-1]

        else:
          result.add tmpl[node.parent.startToken.position.b+1..node.startToken.position.a-1]

      else:
        if node.parent.nodes[i-1].stopToken == nil:
          result.add tmpl[
            node.parent.nodes[i-1].startToken.position.b+1..node.startToken.position.a-1]
        else:
          result.add tmpl[
            node.parent.nodes[i-1].stopToken.position.b+1..node.startToken.position.a-1]

      case node.kind:
        of nkContextVariable:
          result.add node.renderContextVariable(context)

        of nkScopedVariable:
          result.add node.renderScopedVariable(stack)

        of nkIf:
          result.add node.renderIf(context, stack, tmpl)

        of nkElif, nkElse:
          break

        of nkFor:
          result.add node.renderFor(context, stack, tmpl)

        else: discard

      if i == node.parent.nodes.high:
        if node.parent.kind == nkRoot:
          if node.parent.nodes[i].stopToken == nil:
            result.add tmpl[node.startToken.position.b+1..^1]

          else:
            result.add tmpl[node.stopToken.position.b+1..^1]

        else:
          if node.parent.nodes[i].stopToken == nil:
            result.add tmpl[node.startToken.position.b+1..node.parent.stopToken.position.a-1]

          else:
            result.add tmpl[node.stopToken.position.b+1..node.parent.stopToken.position.a-1]


proc render*(tmpl: Template, context: JsonNode): string
    {.raises: [ContextError, TemplateFormatError].} =
  var stack = Stack()

  result = tmpl.node.render(context, stack, tmpl.data)
