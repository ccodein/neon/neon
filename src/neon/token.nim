import std/[with, re, strutils]
import pattern


type
  TokenKind* = enum
    tkScopedVariable
    tkContextVariable
    tkCommentStart
    tkCommentStop
    tkIfStart
    tkIfStop
    tkElseStart
    tkElifStart
    tkForStart
    tkForStop

  Token* = ref object
    kind*: TokenKind
    position*: Slice[int]
    match*: string

  Tokens* = seq[Token]


func `$`*(token: Token): string {.raises: [].} =
  with result:
    add "kind: " & $token.kind & "\n"
    add "match: " & $token.match.escape() & "\n"
    add "position: " & $token.position


func findTokens*(tmpl: string, tokensRegex: TokensRegex): Tokens {.raises: [].} =
  var
    position: tuple[first, last: int]
    token: Token
    startSequence = tokensRegex.startSequence.replace(r"\", "")

  while true:
    token = Token()
    position = tmpl.findBounds(tokensRegex.regex, position.last)

    if position.first == -1:
      break

    token.position = position.first..position.last
    token.match = tmpl[token.position]

    let spaceFreeMatch = token.match.replace(" ", "")

    if spaceFreeMatch.startsWith(startSequence & ">#"):
      token.kind = tkCommentStart
    elif spaceFreeMatch.startsWith(startSequence & "<#"):
      token.kind = tkCommentStop
    elif spaceFreeMatch.startsWith(startSequence & ">if"):
      token.kind = tkIfStart
    elif spaceFreeMatch.startsWith(startSequence & "<if"):
      token.kind = tkIfStop
    elif spaceFreeMatch.startsWith(startSequence & ">elif"):
      token.kind = tkElifStart
    elif spaceFreeMatch.startsWith(startSequence & ">else"):
      token.kind = tkElseStart
    elif spaceFreeMatch.startsWith(startSequence & ">for"):
      token.kind = tkForStart
    elif spaceFreeMatch.startsWith(startSequence & "<for"):
      token.kind = tkForStop
    elif spaceFreeMatch.startsWith(startSequence & "$"):
      token.kind = tkContextVariable
    else:
      token.kind = tkScopedVariable


    result.add(token)

