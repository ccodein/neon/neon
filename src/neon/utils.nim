import std/[json, strutils, enumerate]
import exceptions


func valueByDotNotation*(node: JsonNode, selector: string): JsonNode
    {.raises: [KeyNotExistsError, NotAnObjectError].} =
  let keys = selector.split(".")
  var keyName = keys[0]
  
  result = node

  for i, key in enumerate(keys):
    if result.kind == JObject:
      if i > 0:
        keyName.add("." & key)

      try:
        result = result[key]

      except KeyError:
        raise newException(KeyNotExistsError,
          "The key \"" & keyName & "\" does not exist in the object.")

    else:
      raise newException(NotAnObjectError,
        "The key \"" & keyName & "\" must be of kind JObject.")
